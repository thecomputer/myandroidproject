//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.example.liav.openmanagementsystem.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.models.templates.AccountsContract;
import com.example.liav.openmanagementsystem.models.templates.ProvidersContract;
import com.example.liav.openmanagementsystem.models.templates.SettingsContract;
import com.example.liav.openmanagementsystem.models.user_templates.ProfileSettingsContract;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.CalendarContract;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.MediaContract;
import com.example.liav.openmanagementsystem.models.user_templates.NotesContract;
import com.example.liav.openmanagementsystem.models.user_templates.timetableContract.TimetableContract;

public class DatabaseHelper {
    // Database Manager Methods & Properties for Managing Database

    private SQLiteHelper SQLiteHelper;

    public void restartDB()
    {
        SQLiteDatabase db = SQLiteHelper.getWritableDatabase();
        SQLiteHelper.onUpgrade(db,0,1);

    }

    private boolean validateTable(String table)
    {
        switch (table) {
            case AccountsContract.FeedEntry.TABLE_NAME:
                break;
            case ProvidersContract.FeedEntry.TABLE_NAME:
                break;
            case SettingsContract.FeedEntry.TABLE_NAME:
                break;
            case TimetableContract.FeedEntry.TABLE_NAME:
                break;
            case ProfileSettingsContract.FeedEntry.TABLE_NAME:
                break;
            case CalendarContract.FeedEntry.TABLE_NAME:
                break;
            case MediaContract.FeedEntry.TABLE_NAME:
                break;
            case NotesContract.FeedEntry.TABLE_NAME:
                break;
            default:
                return false;
        }
        return true;
    }
    private ContentValues validateAlteringTable(String table,String[] arr)
    {
        switch (table) {
            case AccountsContract.FeedEntry.TABLE_NAME:
                return AccountsContract.PrepareWriteToTable(arr);
            case ProvidersContract.FeedEntry.TABLE_NAME:
                return ProvidersContract.PrepareWriteToTable(arr);
            case SettingsContract.FeedEntry.TABLE_NAME:
                return SettingsContract.PrepareWriteToTable(arr);

            case TimetableContract.FeedEntry.TABLE_NAME:
                return TimetableContract.PrepareWriteToTable(arr);
            case ProfileSettingsContract.FeedEntry.TABLE_NAME:
                return ProfileSettingsContract.PrepareWriteToTable(arr);
            case CalendarContract.FeedEntry.TABLE_NAME:
                return CalendarContract.PrepareWriteToTable(arr);
            case MediaContract.FeedEntry.TABLE_NAME:
                return MediaContract.PrepareWriteToTable(arr);
            case NotesContract.FeedEntry.TABLE_NAME:
                return NotesContract.PrepareWriteToTable(arr);
            default:
                return null;
        }
    }

    public long InsertRecord(String table,String[] values_arr)
    {
        // open database
        SQLiteDatabase db = SQLiteHelper.getWritableDatabase();
        // prepare new values according to requested table
        ContentValues values;
        values = validateAlteringTable(table,values_arr);
        if(values == null)
            return -1; //error
        
        long newRowId = db.insert(table, null, values);
        return newRowId;
    }
    public long GetAmountOfRecords(String table)
    {
        Cursor cursor = this.getRecords(table,"1","1",null);
        return cursor.getCount();
    }
    public long UpdateRecord(String table,String[] new_values_arr, long id)
    {
        // open database
        SQLiteDatabase db = SQLiteHelper.getWritableDatabase();
        // prepare new values according to requested table
        ContentValues values;
        values = validateAlteringTable(table,new_values_arr);
        if(values == null)
            return -1; //error


        // prepare selector
        // Which row to update, based on the title
        String selection = "_id" + "=?";
        String[] selectionArgs = { ""+id+"" };

        int idUpdated = db.update(
                table,
                values,
                selection,
                selectionArgs);

        return idUpdated;
    }



    public int DeleteRecord(String table,long id)
    {
        // open database
        SQLiteDatabase db = SQLiteHelper.getWritableDatabase();
        // prepare selector
        String selection = "_id" + "=?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = {""+id+""};
        // Issue SQL statement.
        int deletedRows = db.delete(table, selection, selectionArgs);
        return deletedRows;
        //return -1;
    }
    public int FindRecord(String table,String column,String value)
    {
        SQLiteDatabase db = SQLiteHelper.getReadableDatabase();

    // Define a projection that specifies which columns from the database
    // you will actually use after this query.
        String[] projection = {
                BaseColumns._ID
        };

    // Filter results WHERE "title" = 'My Title'
        String selection = column + " = ?";
        String[] selectionArgs = { value };

        if(!validateTable(table))
            return -1;
    // How you want the results sorted in the resulting Cursor
        Cursor cursor;
            cursor = db.rawQuery("SELECT * FROM " + table + " WHERE " + column + " = " + value,null);

        //cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex(BaseColumns._ID));

    }
    public Cursor getRecords(String table,String column,String value, String sortOrder)
    {
        SQLiteDatabase db = SQLiteHelper.getReadableDatabase();

        // Filter results WHERE "title" = 'My Title'
        //String selection = column + " = ?";
        //String[] selectionArgs = { value };

        if(!validateTable(table))
            return null;
        // How you want the results sorted in the resulting Cursor
        Cursor cursor;
        if(sortOrder == null)
        {
             cursor = db.rawQuery("SELECT * FROM " + table + " WHERE " + column + " = " + value,null);
        }
        else {
            cursor = db.rawQuery("SELECT * FROM " + table + " WHERE " + column + " = " + value + " " + sortOrder, null);
        }

        cursor.moveToPosition(0);
        //cursor.moveToFirst();
        return cursor;
    }

    public Cursor getRecordsAfterTimeStamp(String table,String column,int value, String sortOrder)
    {
        SQLiteDatabase db = SQLiteHelper.getReadableDatabase();

        if(!validateTable(table))
            return null;
        // How you want the results sorted in the resulting Cursor
        Cursor cursor;
        if(sortOrder == null)
        {
            cursor = db.rawQuery("SELECT * FROM " + table + " WHERE " + column + " > " + value,null);
        }
        else {
            cursor = db.rawQuery("SELECT * FROM " + table + " WHERE " + column + " > " + value + " " + sortOrder, null);
        }

        //cursor.moveToFirst();
        return cursor;
    }
    public Cursor getRecordsBetweenTimeStamps(String table,String column,int value1,int value2, String sortOrder)
    {
        SQLiteDatabase db = SQLiteHelper.getReadableDatabase();

        if(!validateTable(table))
            return null;
        // How you want the results sorted in the resulting Cursor
        Cursor cursor;
        if(sortOrder == null)
        {
            cursor = db.rawQuery("SELECT * FROM " + table + " WHERE " + column + " >= " + value1 + " AND " + column + " <= " + value2,null);
        }
        else {
            cursor = db.rawQuery("SELECT * FROM " + table + " WHERE " + column + " >= " + value1 + " AND " + column + " <= " + value2 + " " + sortOrder, null);
        }

        //cursor.moveToFirst();
        return cursor;
    }

    public DatabaseHelper(Context context) {
        SQLiteHelper SQLiteHelper = new SQLiteHelper(context);
        this.SQLiteHelper = SQLiteHelper;
    }
}
