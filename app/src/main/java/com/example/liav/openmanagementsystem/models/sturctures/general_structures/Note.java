package com.example.liav.openmanagementsystem.models.sturctures.general_structures;

public class Note {
    private int unix_timestamp;
    private String title;
    private String desc;
    private int id;

    public Note(int id,int unix_timestamp, String title, String desc) {
        this.unix_timestamp = unix_timestamp;
        this.title = title;
        this.desc = desc;
        this.id = id;
    }

    public int getUnix_timestamp() {
        return unix_timestamp;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    public int getId() {
        return id;
    }
}
