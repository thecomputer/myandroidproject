package com.example.liav.openmanagementsystem.utils;

//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.CalendarEventsListFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.TimetableEventsListFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments.NotesFragment;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.templates.NotesFragmentTypes;
import com.example.liav.openmanagementsystem.models.user_templates.NotesContract;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.CalendarContract;
import com.example.liav.openmanagementsystem.models.user_templates.timetableContract.TimetableContract;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.network.AsyncHTTP_Post;
import com.example.liav.openmanagementsystem.network.AsyncHTTP_RequestJSON;
import com.example.liav.openmanagementsystem.network.HTTPHelper;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;
import com.example.liav.openmanagementsystem.utils.timezone.TimezoneHelper;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;

public class UserDataManager {

    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor PrefEditor;

    private DatabaseHelper dbManager;
    public TimezoneHelper timezoneHelper;

    private String url;
    private String token;

    // Shared preferences

    private void InitalizeSharedPreferences() {
        this.sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        this.PrefEditor = this.sharedPref.edit();
    }

    public void InsertBooleanSharedPreference(String key, Boolean value) {
        this.PrefEditor.putBoolean(key, value);
        this.PrefEditor.commit();
    }

    public void InsertStringSharedPreference(String key, String value) {
        this.PrefEditor.putString(key, value);
        this.PrefEditor.commit();
    }

    public void InsertLongSharedPreference(String key, Long value) {
        this.PrefEditor.putLong(key, value);
        this.PrefEditor.commit();
    }

    public void InsertIntSharedPreference(String key, int value) {
        this.PrefEditor.putInt(key, value);
        this.PrefEditor.commit();
    }

    public void InsertFloatSharedPreference(String key, Float value) {
        this.PrefEditor.putFloat(key, value);
        this.PrefEditor.commit();
    }


    public String ReturnStringSharedPreference(String key) {
        return sharedPref.getString(key, "");
    }

    public int ReturnIntSharedPreference(String key) {
        return sharedPref.getInt(key, 0);
    }

    public Float ReturnFloatSharedPreference(String key) {
        return sharedPref.getFloat(key, 0);
    }

    public Long ReturnLongSharedPreference(String key) {
        return sharedPref.getLong(key, 0);
    }

    public Boolean ReturnBooleanSharedPreference(String key) {
        return sharedPref.getBoolean(key, false);
    }

    public void DeleteSharedPreference(String key) {
        this.PrefEditor.remove(key);
        this.PrefEditor.apply();
    }

    public void ClearSharedPreferences() {
        this.PrefEditor.clear();
        this.PrefEditor.apply();
    }

    public void RemoveSharedPreferences() {
        ArrayList<String> keys = new ArrayList<String>();
        sharedPref.getAll();
        this.PrefEditor.apply();
    }
    //// All Initializing Functions here ////

    public UserDataManager(Context context1) {
        context = context1;
        dbManager = CreateDatabaseManager();
        //dbManager.restartDB();

        //settings = UpdateSettings();
        //Account account = new Account("default","default","default","default","default");

        //Log.i("working:",""+RevertDefaultSettings());
        //InitalizeSharedPreferences();
        Properties properties = AppDataHolder.getInstance().getData().UpdateSettings();
        if (properties != null)
        {
            if(properties.getProperty("timezone").equals("0"))
            {
                this.timezoneHelper = new TimezoneHelper(TimezoneHelper.DEFAULT_ANDROID_TIMEZONE);
            }
            else if (properties.getProperty("timezone").equals("1"))
            {
                this.timezoneHelper = new TimezoneHelper(TimezoneHelper.REQUEST_GPS_USAGE);
            }
        }

        //this.url = "http://10.0.0.47/";
        //this.token="44b5d7ac6221443779f20e0d8bab34aa";

    }



    private DatabaseHelper CreateDatabaseManager() {
        DatabaseHelper db_interact = new DatabaseHelper(this.context);
        return db_interact;
    }



    //// All Local Database interactions here ////


    /*public Properties UpdateSettings() {

        Properties settings = new Properties();
        Cursor cursor = dbManager.getRecords("Settings", "1","1",null);
        if (cursor.getCount() == 0)
            return settings;
        while (cursor.moveToNext()) {

            settings.put(cursor.getString(cursor.getColumnIndex(SettingsContract.columns[0])), cursor.getString(cursor.getColumnIndex(SettingsContract.columns[1])));
        }
        return settings;
    }
    public void PrintProperties()
    {
        Log.i("nothing",""+settings.isEmpty());
        Log.i("Printing msg","nothing");
        String str;
        Set properites = settings.keySet();   // get set-view of keys
        Iterator itr = properites.iterator();

        while(itr.hasNext()) {
            str = (String) itr.next();
            Log.i("Printing msg","The setting of " + str + " is " + settings.getProperty(str) + ".");
        }
    }*/

    ///

    public ArrayList<Note> getNotes(int minimum_timestamp, int category, int orderBy, NotesFragment fragment)
    {
        ArrayList<Note> eventArrayList = new ArrayList<>();
        if(isLoggedLocal)
        {
            Cursor cursor = null;
            switch(category)
            {
                case (NotesFragmentTypes.SelectNotes._DATE_SELECTOR):

                    if(orderBy == NotesFragmentTypes.SelectNotes.DATE.DESC_ORDER)
                    {
                        cursor = this.dbManager.getRecordsAfterTimeStamp(NotesContract.FeedEntry.TABLE_NAME,
                                NotesContract.FeedEntry.COLUMN1_NAME_TITLE,
                                minimum_timestamp,
                                ("ORDER BY " + NotesContract.FeedEntry.COLUMN1_NAME_TITLE + " DESC"));
                    }
                    else if(orderBy == NotesFragmentTypes.SelectNotes.DATE.ASC_ORDER)
                    {
                        cursor = this.dbManager.getRecordsAfterTimeStamp(NotesContract.FeedEntry.TABLE_NAME,
                                NotesContract.FeedEntry.COLUMN1_NAME_TITLE,
                                minimum_timestamp,
                                ("ORDER BY " + NotesContract.FeedEntry.COLUMN1_NAME_TITLE + " ASC"));
                    }
                    break;

                case (NotesFragmentTypes.SelectNotes._TITLE_SELECTOR):
                    if(orderBy == NotesFragmentTypes.SelectNotes.DATE.DESC_ORDER)
                    {
                        cursor = this.dbManager.getRecordsAfterTimeStamp(NotesContract.FeedEntry.TABLE_NAME,
                                NotesContract.FeedEntry.COLUMN1_NAME_TITLE,
                                minimum_timestamp,
                                ("ORDER BY " + NotesContract.FeedEntry.COLUMN2_NAME_TITLE + " DESC"));
                    }
                    else if(orderBy == NotesFragmentTypes.SelectNotes.DATE.ASC_ORDER)
                    {
                        cursor = this.dbManager.getRecordsAfterTimeStamp(NotesContract.FeedEntry.TABLE_NAME,
                                NotesContract.FeedEntry.COLUMN1_NAME_TITLE,
                                minimum_timestamp,
                                ("ORDER BY " + NotesContract.FeedEntry.COLUMN2_NAME_TITLE + " ASC"));
                    }
                    break;
            }




            if (cursor.getCount() == 0)
            {
                return eventArrayList;
            }

            cursor.moveToPosition(0);

            while (true) {
                Note t = new Note(
                        cursor.getInt(cursor.getColumnIndex(NotesContract.FeedEntry._ID)),
                        cursor.getInt(cursor.getColumnIndex((NotesContract.FeedEntry.COLUMN1_NAME_TITLE))),
                        cursor.getString(cursor.getColumnIndex(NotesContract.FeedEntry.COLUMN2_NAME_TITLE)),
                        cursor.getString(cursor.getColumnIndex(NotesContract.FeedEntry.COLUMN3_NAME_TITLE))
                        );
                eventArrayList.add(t);
                if(cursor.isLast())
                    break;
                cursor.moveToNext();
            }

            return eventArrayList;
        }
        else
        {

            String url = null;
            switch(category)
            {
                case (NotesFragmentTypes.SelectNotes._DATE_SELECTOR):

                    if(orderBy == NotesFragmentTypes.SelectNotes.DATE.DESC_ORDER)
                    {
                        url = new String(this.url+"getdata.php?get=note_filtered&token="+this.token+"&start_timestamp="+minimum_timestamp+"&sort=unixtimestamp&orderBy=DESC");
                    }
                    else if(orderBy == NotesFragmentTypes.SelectNotes.DATE.ASC_ORDER)
                    {
                        url = new String(this.url+"getdata.php?get=note_filtered&token="+this.token+"&start_timestamp="+minimum_timestamp+"&sort=unixtimestamp&orderBy=ASC");
                    }
                    break;

                case (NotesFragmentTypes.SelectNotes._TITLE_SELECTOR):
                    if(orderBy == NotesFragmentTypes.SelectNotes.DATE.DESC_ORDER)
                    {
                        url = new String(this.url+"getdata.php?get=note_filtered&token="+this.token+"&start_timestamp="+minimum_timestamp+"&sort=title&orderBy=DESC");
                    }
                    else if(orderBy == NotesFragmentTypes.SelectNotes.DATE.ASC_ORDER)
                    {
                        url = new String(this.url+"getdata.php?get=note_filtered&token="+this.token+"&start_timestamp="+minimum_timestamp+"&sort=title&orderBy=ASC");
                    }
                    break;
            }
            Log.i("str",url);
            HTTPHelper.RequestDataHTTP(url,AsyncHTTP_RequestJSON.NOTES_TYPE,fragment);

            return eventArrayList;
        }

    }

    public ArrayList<TimetableEvent> getDayEvent(int day,TimetableEventsListFragment fragment)
    {
        final ArrayList<TimetableEvent> eventArrayList = new ArrayList<>();
        if(isLoggedLocal)
        {
            String day_string = ""+day;

            Cursor cursor = this.dbManager.getRecords(TimetableContract.FeedEntry.TABLE_NAME,
                    TimetableContract.FeedEntry.COLUMN1_NAME_TITLE,
                    day_string,
                    ("ORDER BY " + TimetableContract.FeedEntry.COLUMN2_NAME_TITLE + "," + TimetableContract.FeedEntry.COLUMN3_NAME_TITLE + " ASC"));
            if (cursor.getCount() == 0)
            {
                return eventArrayList;
            }

            cursor.moveToPosition(0);
            while (true) {
                TimetableEvent t = new TimetableEvent(
                        cursor.getInt(cursor.getColumnIndex(TimetableContract.FeedEntry._ID)),
                        false,
                        cursor.getString(cursor.getColumnIndex(TimetableContract.columns[3])),
                        cursor.getString(cursor.getColumnIndex(TimetableContract.columns[4])),
                        cursor.getString(cursor.getColumnIndex(TimetableContract.columns[5])),
                        cursor.getInt(cursor.getColumnIndex(TimetableContract.columns[0])),
                        cursor.getInt(cursor.getColumnIndex(TimetableContract.columns[1])),
                        cursor.getInt(cursor.getColumnIndex(TimetableContract.columns[2]))
                );
                eventArrayList.add(t);
                if(cursor.isLast())
                    break;
                cursor.moveToNext();
            }

            return eventArrayList;
        }
        else
        {
            String url = new String(this.url+"getdata.php?get=timetable&token="+this.token+"&day="+day);
            Log.i("str",url);
            Log.i("day",""+day);
            HTTPHelper.RequestDataHTTP(url, AsyncHTTP_RequestJSON.TIMETABLE_TYPE,fragment);

            return eventArrayList;
        }
    }

    public ArrayList<CalendarEvent> getAllCalendarEvent(CalendarEventsListFragment fragment)
    {
        ArrayList<CalendarEvent> calendarEventArrayList = new ArrayList<>();
        if(isLoggedLocal)
        {
            Cursor cursor = this.dbManager.getRecords(CalendarContract.FeedEntry.TABLE_NAME,
                    "1",
                    "1",
                    ("ORDER BY " + CalendarContract.FeedEntry.COLUMN1_NAME_TITLE + " DESC"));
            if (cursor.getCount() == 0)
            {
                return calendarEventArrayList;
            }

            cursor.moveToPosition(0);
            while (true) {
                CalendarEvent t = new CalendarEvent(
                        cursor.getInt(cursor.getColumnIndex(CalendarContract.FeedEntry._ID)),
                        false,
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN3_NAME_TITLE)),
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN4_NAME_TITLE)),
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN5_NAME_TITLE)),
                        cursor.getLong(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN1_NAME_TITLE))
                );
                calendarEventArrayList.add(t);
                if(cursor.isLast())
                    break;
                cursor.moveToNext();
            }

            return calendarEventArrayList;
        }
        else
        {
            String url = new String(this.url+"getdata.php?get=calendar&token="+this.token);
            Log.i("str",url);
            HTTPHelper.RequestDataHTTP(url, AsyncHTTP_RequestJSON.CALENDAR_TYPE,fragment);
            return calendarEventArrayList;
        }

    }

    public ArrayList<CalendarEvent> getTodayCalendarEvent(CalendarEventsListFragment fragment)
    {
        Calendar cal = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
        int[] timestamps = getStartStopTimestamps(cal);

        Log.i("date","start: "+timestamps[0]);
        Log.i("date","stop: "+timestamps[1]);


        ArrayList<CalendarEvent> calendarEventArrayList = new ArrayList<>();
        if(isLoggedLocal)
        {
            Cursor cursor = this.dbManager.getRecordsBetweenTimeStamps(CalendarContract.FeedEntry.TABLE_NAME,
                    CalendarContract.FeedEntry.COLUMN1_NAME_TITLE,
                    timestamps[0],timestamps[1],null);
            if (cursor.getCount() == 0)
            {
                return calendarEventArrayList;
            }

            cursor.moveToPosition(0);
            while (true) {
                CalendarEvent t = new CalendarEvent(
                        cursor.getInt(cursor.getColumnIndex(CalendarContract.FeedEntry._ID)),
                        false,
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN3_NAME_TITLE)),
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN4_NAME_TITLE)),
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN5_NAME_TITLE)),
                        cursor.getLong(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN1_NAME_TITLE))
                );
                calendarEventArrayList.add(t);
                if(cursor.isLast())
                    break;
                cursor.moveToNext();
            }

            return calendarEventArrayList;
        }
        else
        {
            String url = new String(this.url+"getdata.php?get=calendar_filtered&token="+this.token+"&start_timestamp="+timestamps[0]+"&stop_timestamp="+timestamps[1]);
            Log.i("str",url);
            HTTPHelper.RequestDataHTTP(url, AsyncHTTP_RequestJSON.CALENDAR_TYPE,fragment);
            return calendarEventArrayList;
        }

    }

    private int[] getStartStopTimestamps(Calendar cal)
    {
        cal.set(Calendar.HOUR_OF_DAY,0);
        cal.set(Calendar.MINUTE,0);
        int startTimestamp = (int)(cal.getTimeInMillis()/1000);
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        int endTimestamp = (int)(cal.getTimeInMillis()/1000);
        int[] timestamps = {startTimestamp,endTimestamp};
        return timestamps;
    }




    public ArrayList<CalendarEvent> getDayCalendarEvent(int day,int month, int year,CalendarEventsListFragment fragment)
    {
        Calendar cal = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
        cal.set(Calendar.DAY_OF_MONTH,day);
        cal.set(Calendar.MONTH,month);
        cal.set(Calendar.YEAR,year);
        Log.i("date",""+day+" "+month+" "+year);

        int[] timestamps = getStartStopTimestamps(cal);

        Log.i("date","start: "+timestamps[0]);
        Log.i("date","stop: "+timestamps[1]);


        ArrayList<CalendarEvent> calendarEventArrayList = new ArrayList<>();
        if(isLoggedLocal)
        {
            Cursor cursor = this.dbManager.getRecordsBetweenTimeStamps(CalendarContract.FeedEntry.TABLE_NAME,
                    CalendarContract.FeedEntry.COLUMN1_NAME_TITLE,
                    timestamps[0],timestamps[1],null);
            if (cursor.getCount() == 0)
            {
                return calendarEventArrayList;
            }

            cursor.moveToPosition(0);

            while (true) {
                CalendarEvent t = new CalendarEvent(
                        cursor.getInt(cursor.getColumnIndex(CalendarContract.FeedEntry._ID)),
                        false,
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN3_NAME_TITLE)),
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN4_NAME_TITLE)),
                        cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN5_NAME_TITLE)),
                        cursor.getLong(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN1_NAME_TITLE))
                );
                calendarEventArrayList.add(t);
                if(cursor.isLast())
                    break;
                cursor.moveToNext();
            }

            return calendarEventArrayList;
        }
        else
        {
            // http://10.0.0.47/getdata.php?get=calendar_filtered&token=531dfqwe34&start_timestamp=555&stop_timestamp=3333
            String url = new String(this.url+"getdata.php?get=calendar_filtered&token="+this.token+"&start_timestamp="+timestamps[0]+"&stop_timestamp="+timestamps[1]);
            Log.i("str",url);
            HTTPHelper.RequestDataHTTP(url, AsyncHTTP_RequestJSON.CALENDAR_TYPE,fragment);
            return calendarEventArrayList;
        }

    }

    /*
    public Note getNoteValues(int id,CalendarEventsListFragment fragment)
    {
        Note t = null;
        if(isLoggedLocal)
        {


            Cursor cursor = this.dbManager.getRecords(NotesContract.FeedEntry.TABLE_NAME, "1","1",null);
            if (cursor.getCount() == 0)
            {
                return null;
            }
            while (true) {

                //eventArrayList.put(cursor.getString(cursor.getColumnIndex(SettingsContract.columns[0])), cursor.getString(cursor.getColumnIndex(SettingsContract.columns[1])));
                if(cursor.getInt(cursor.getColumnIndex(NotesContract.FeedEntry._ID)) == id) {
                    t = new Note(
                            cursor.getInt(cursor.getColumnIndex(NotesContract.FeedEntry._ID)),
                            cursor.getInt(cursor.getColumnIndex(NotesContract.FeedEntry.COLUMN1_NAME_TITLE)),
                            cursor.getString(cursor.getColumnIndex(NotesContract.FeedEntry.COLUMN2_NAME_TITLE)),
                            cursor.getString(cursor.getColumnIndex(NotesContract.FeedEntry.COLUMN3_NAME_TITLE))
                    );
                    break;
                }
                cursor.moveToNext();
            }

        }
        else
        {
            String url = new String("http://10.0.0.47/getdata.php?get=calendar&token=531dfqwe34");
            Log.i("str",url);
            HTTPHelper.RequestDataHTTP(url, AsyncHTTP_RequestJSON.CALENDAR_TYPE,fragment);
            return t;
        }
    }
    */

    /*public CalendarEvent getCalendarEventValues(int id)
    {
        CalendarEvent t = null;
        if(isLoggedLocal)
        {


            Cursor cursor = this.dbManager.getRecords(CalendarContract.FeedEntry.TABLE_NAME, "1","1",null);
            if (cursor.getCount() == 0)
            {
                return null;
            }
            while (true) {

                //eventArrayList.put(cursor.getString(cursor.getColumnIndex(SettingsContract.columns[0])), cursor.getString(cursor.getColumnIndex(SettingsContract.columns[1])));
                if(cursor.getInt(cursor.getColumnIndex(CalendarContract.FeedEntry._ID)) == id) {
                    t = new CalendarEvent(
                            cursor.getInt(cursor.getColumnIndex(CalendarContract.FeedEntry._ID)),
                            false,
                            cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN3_NAME_TITLE)),
                            cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN4_NAME_TITLE)),
                            cursor.getString(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN5_NAME_TITLE)),
                            cursor.getLong(cursor.getColumnIndex(CalendarContract.FeedEntry.COLUMN1_NAME_TITLE))
                    );
                    break;
                }
                cursor.moveToNext();
            }

        }
        return t;

    }*/

    /*public TimetableEvent getDayEventValues(int id)
    {
        TimetableEvent t = null;
        if(isLoggedLocal)
        {


            Cursor cursor = this.dbManager.getRecords(TimetableContract.FeedEntry.TABLE_NAME, "1","1",null);
            if (cursor.getCount() == 0)
            {
                return null;
            }
            while (true) {

                //eventArrayList.put(cursor.getString(cursor.getColumnIndex(SettingsContract.columns[0])), cursor.getString(cursor.getColumnIndex(SettingsContract.columns[1])));
                if(cursor.getInt(cursor.getColumnIndex(TimetableContract.FeedEntry._ID)) == id) {
                    t = new TimetableEvent(
                            cursor.getInt(cursor.getColumnIndex(TimetableContract.FeedEntry._ID)),
                            false,
                            cursor.getString(cursor.getColumnIndex(TimetableContract.columns[3])),
                            cursor.getString(cursor.getColumnIndex(TimetableContract.columns[4])),
                            cursor.getString(cursor.getColumnIndex(TimetableContract.columns[5])),
                            cursor.getInt(cursor.getColumnIndex(TimetableContract.columns[0])),
                            cursor.getInt(cursor.getColumnIndex(TimetableContract.columns[1])),
                            cursor.getInt(cursor.getColumnIndex(TimetableContract.columns[2]))
                    );
                    break;
                }
                cursor.moveToNext();
            }

        }


        return t;

    }*/

    ////


    public boolean createTimetableEvent(HashMap<String,String> values)
    {
        if(isLoggedLocal)
        {
            String[] values_arr = {
                    values.get(TimetableContract.FeedEntry.COLUMN1_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN2_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN3_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN4_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN5_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN6_NAME_TITLE)

            };
            this.dbManager.InsertRecord(TimetableContract.FeedEntry.TABLE_NAME,values_arr);
        }
        else
        {
            String url = new String(this.url+"postdata.php?set=timetable_event&token="+this.token);
            Log.i("str",url);
            HTTPHelper.PostDataHTTP(values,url, AsyncHTTP_Post.TIMETABLE_TYPE);
        }
        return true;
    }
    public boolean createCalendarEvent(HashMap<String,String> values)
    {
        if(isLoggedLocal) {
            String[] values_arr = {
                    values.get(CalendarContract.FeedEntry.COLUMN1_NAME_TITLE),
                    values.get(CalendarContract.FeedEntry.COLUMN2_NAME_TITLE),
                    values.get(CalendarContract.FeedEntry.COLUMN3_NAME_TITLE),
                    values.get(CalendarContract.FeedEntry.COLUMN4_NAME_TITLE),
                    values.get(CalendarContract.FeedEntry.COLUMN5_NAME_TITLE),
            };
            this.dbManager.InsertRecord(CalendarContract.FeedEntry.TABLE_NAME, values_arr);
        }
        else
        {
            String url = new String(this.url+"postdata.php?set=calendar_event&token="+this.token);
            Log.i("str",url);
            HTTPHelper.PostDataHTTP(values,url, AsyncHTTP_Post.CALENDAR_TYPE);
        }
        return true;
    }


    public boolean createNotes(Note note)
    {
        if(isLoggedLocal) {
            String[] arr = {"" + note.getUnix_timestamp(), note.getTitle(), note.getDesc()};
            this.dbManager.InsertRecord(NotesContract.FeedEntry.TABLE_NAME, arr);
        }
        else
        {
            String url = new String(this.url+"postdata.php?set=note&token="+this.token);
            Log.i("str",url);
            HashMap<String,String> values = new HashMap<>();
            values.put(NotesContract.FeedEntry.COLUMN2_NAME_TITLE,note.getTitle());
            values.put(NotesContract.FeedEntry.COLUMN3_NAME_TITLE,note.getDesc());

            HTTPHelper.PostDataHTTP(values,url, AsyncHTTP_Post.NOTES_TYPE);
        }
        return true;

    }


    //////

    public boolean editTimetableEvent(int id,HashMap<String,String> values)
    {
        if(isLoggedLocal) {
            String[] values_arr = {
                    values.get(TimetableContract.FeedEntry.COLUMN1_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN2_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN3_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN4_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN5_NAME_TITLE),
                    values.get(TimetableContract.FeedEntry.COLUMN6_NAME_TITLE)
            };
            this.dbManager.UpdateRecord(TimetableContract.FeedEntry.TABLE_NAME, values_arr, (long) id);
        }
        else
        {
            String url = new String(this.url+"updatedata.php?set=timetable_event&id="+id+"&token="+this.token);
            Log.i("str",url);
            HTTPHelper.PostDataHTTP(values,url, AsyncHTTP_Post.TIMETABLE_TYPE);
        }
        return true;
    }

    public boolean editCalendarEvent(int id,HashMap<String,String> values)
    {
        if(isLoggedLocal) {
            String[] values_arr = {
                    values.get(CalendarContract.FeedEntry.COLUMN1_NAME_TITLE),
                    values.get(CalendarContract.FeedEntry.COLUMN2_NAME_TITLE),
                    values.get(CalendarContract.FeedEntry.COLUMN3_NAME_TITLE),
                    values.get(CalendarContract.FeedEntry.COLUMN4_NAME_TITLE),
                    values.get(CalendarContract.FeedEntry.COLUMN5_NAME_TITLE)
            };

            this.dbManager.UpdateRecord(CalendarContract.FeedEntry.TABLE_NAME, values_arr, (long) id);
        }
        else
        {
            String url = new String(this.url+"updatedata.php?set=calendar_event&id="+id+"&token="+this.token);
            Log.i("str",url);
            HTTPHelper.PostDataHTTP(values,url, AsyncHTTP_Post.CALENDAR_TYPE);
        }
        return true;

    }
    public boolean editNote(int id,HashMap<String,String> values)
    {
        if(isLoggedLocal) {

            String[] values_arr = {
                    values.get(NotesContract.FeedEntry.COLUMN1_NAME_TITLE),
                    values.get(NotesContract.FeedEntry.COLUMN2_NAME_TITLE),
                    values.get(NotesContract.FeedEntry.COLUMN3_NAME_TITLE)
            };
            this.dbManager.UpdateRecord(NotesContract.FeedEntry.TABLE_NAME, values_arr, (long) id);
        }
        else
        {
            String url = new String(this.url+"updatedata.php?set=note&id="+id+"&token="+this.token);
            Log.i("str",url);
            Log.i("str",url);
            HTTPHelper.PostDataHTTP(values,url, AsyncHTTP_Post.NOTES_TYPE);
        }
        return true;
    }

    //////

    public boolean deleteTimetableEvent(int id)
    {
        if(isLoggedLocal)
            this.dbManager.DeleteRecord(TimetableContract.FeedEntry.TABLE_NAME,(long)id);
        else
        {
            String url = new String(this.url+"removedata.php?del=timetable&id="+id+"&token="+this.token);
            Log.i("str",url);
            Log.i("str",url);
            HTTPHelper.UpdateDataHTTP(url);
        }
        return true;
    }
    public boolean deleteCalendarEvent(int id)
    {
        if(isLoggedLocal)
            this.dbManager.DeleteRecord(CalendarContract.FeedEntry.TABLE_NAME,(long)id);
        else
        {
            String url = new String(this.url+"removedata.php?del=calendar&id="+id+"&token="+this.token);
            Log.i("str",url);
            Log.i("str",url);
            HTTPHelper.UpdateDataHTTP(url);

        }
        return true;

    }
    public boolean deleteNote(int id)
    {
        if(isLoggedLocal)
            this.dbManager.DeleteRecord(NotesContract.FeedEntry.TABLE_NAME,(long)id);
        else
        {
            String url = new String(this.url+"removedata.php?del=note&id="+id+"&token="+this.token);
            Log.i("str",url);
            Log.i("str",url);
            HTTPHelper.UpdateDataHTTP(url);
        }
        return true;
    }




    ////////////////////////////////////
    ////////////////////////////////////
    // All Account interactions here ///
    ////////////////////////////////////
    ////////////////////////////////////

    private boolean isLoggedLocal = false;
    private Account currentSession;

    public void SetAccount(Account account)
    {
        this.currentSession = account;
        Log.i("new login",account.getProvider_uri());
        if(account.getProvider_uri().equals("local://localphone/"))
        {
            LoginLocalUser();
        }
        else
        {
            LoginRemoteUser(account);
            Log.i("login",this.currentSession.getToken());
        }
    }

    public Account getAccountSession()
    {
        return new Account(
                this.currentSession.getUsername(),
                this.currentSession.getPassword(),
                this.currentSession.getEmail(),
                this.currentSession.getProvider_uri(),
                this.currentSession.getHash_strength_provider()
        );
    }

    private boolean LoginRemoteUser(Account account_instance)
    {
        // stub, will return true for now
        //dbManager.InsertRecord("Accounts",account.getValues());
        this.isLoggedLocal = false;
        this.token = this.currentSession.getToken();
        this.url = this.currentSession.getProvider_uri();
        //HTTPHelper.LoginHTTP(account_instance);
        return true;
    }
    private boolean LoginLocalUser()
    {
        this.isLoggedLocal = true;
        return true;
    }

    public boolean LogoutUser()
    {
        UserDataHolder.getInstance().setData(null);
        return true;
    }

    /////////////////////////////////////
    /////////////////////////////////////
    /////////////////////////////////////
    // All Settings interactions here ///
    /////////////////////////////////////
    /////////////////////////////////////
/*
    public boolean ChangeSettings(Properties settings) {
        String[] values=new String[4];
        String str;

        Set properties = settings.keySet();   // get set-view of keys
        Iterator itr = properties.iterator();

        int i=0;

        while(itr.hasNext()) {
            str = (String) itr.next();
            values[0] = SettingsContract.DefaultSettings[i][0];
            values[2] = SettingsContract.DefaultSettings[i][2];
            values[1] = settings.getProperty(str);
            values[3] = SettingsContract.DefaultSettings[i][3];
            long s = dbManager.UpdateRecord("Settings", values, i+1);
            Log.i("Printing msg" + i  + "","The setting of " + str + " is " + settings.getProperty(str) + ".");
            i++;
        }

        return true;
    }

    public Cursor ListSettings() {
        return dbManager.getRecords("Settings", "1", "1", null);
    }

    public boolean RevertDefaultSettings() {
        int i=0;
        for (i = 0; i < SettingsContract.DefaultSettings.length; i++) {
            String[] values_arr = {SettingsContract.DefaultSettings[i][0], SettingsContract.DefaultSettings[i][1], SettingsContract.DefaultSettings[i][2],SettingsContract.DefaultSettings[i][3]};
            long s = dbManager.UpdateRecord("Settings", values_arr, i+1);
            Log.i("Settings updated",""+s);
            if (s == -1)
                return false;
            if (s==0)
            {
                dbManager.InsertRecord("Settings",values_arr);
            }
        }
        while(i<dbManager.GetAmountOfRecords("Settings"))
        {
            dbManager.DeleteRecord("Settings",i+1);
        }
        return true;
    }*/

    //// All Remote Data interactions here




}

