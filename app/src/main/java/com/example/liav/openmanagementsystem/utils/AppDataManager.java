//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.example.liav.openmanagementsystem.utils;

import com.example.liav.openmanagementsystem.models.templates.AccountsContract;
import com.example.liav.openmanagementsystem.models.templates.ProvidersContract;
import com.example.liav.openmanagementsystem.models.templates.SettingsContract;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Provider;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class AppDataManager {

    private Context context;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor PrefEditor;

    private Properties settings;

    private DatabaseHelper dbManager;
    private boolean NetworkConnected;


//// All Initializing Functions here ////

    public Context getContext()
    {
        return this.context;
    }

    public AppDataManager(Context context1) {
        this.context = context1;
        this.dbManager = CreateDatabaseManager();
        //this.dbManager.restartDB();
        //accounts = retrieveAccounts();
        //accounts = retrieveAccounts();
        //providers = retrieveProviders();
        this.settings = UpdateSettings();

        if(this.dbManager.GetAmountOfRecords("Accounts") == 0)
        {
            Account account = new Account("default","default","default","default","default");
            CreateAccount(account);
        }


        Log.i("working:",""+RevertDefaultSettings());

        //this.settings.setProperty("allow_insecure_connections","true");
        //this.settings.setProperty("allow_adminmode","true");
        //ChangeSettings(this.settings);

    }


    private DatabaseHelper CreateDatabaseManager() {
        DatabaseHelper db_interact = new DatabaseHelper(this.context);
        return db_interact;
    }



    private ArrayList<Account> retrieveAccounts() {
        return UpdateAccountsArray();
    }

    private ArrayList<Provider> retrieveProviders() {
        return UpdateProvidersArray();
    }

    //// All Local Database interactions here ////


    public Properties UpdateSettings() {

        Properties settings = new Properties();
        Cursor cursor = dbManager.getRecords("Settings", "1","1",null);
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            settings.put(cursor.getString(cursor.getColumnIndex(SettingsContract.FeedEntry.COLUMN2_NAME_TITLE)), cursor.getString(cursor.getColumnIndex(SettingsContract.FeedEntry.COLUMN3_NAME_TITLE)));
        }
        return settings;
    }



    /*public String GetSetting() {
        Cursor cursor = dbManager.getRecords("Settings", SettingsContract.FeedEntry.COLUMN1_NAME_TITLE,SettingsContract.SettingsTypes,null);
        if (cursor.getCount() == 0)
            return settings;
        while (cursor.moveToNext()) {

            settings.put(cursor.getString(cursor.getColumnIndex(SettingsContract.columns[0])), cursor.getString(cursor.getColumnIndex(SettingsContract.columns[1])));
        }
        return settings;
    }*/

    public void PrintProperties(Properties settings)
    {
        Log.i("nothing",""+settings.isEmpty());
        Log.i("Printing msg","nothing");
        String str;
        Set properties = settings.keySet();   // get set-view of keys
        Iterator itr = properties.iterator();

        while(itr.hasNext()) {
            str = (String) itr.next();
            Log.i("Printing msg","The setting of " + str + " is " + settings.getProperty(str) + ".");
        }
    }


    public void setNetworkConnected(boolean networkConnected) {
        this.NetworkConnected = networkConnected;
    }

    public boolean isNetworkConnected() {
        return this.NetworkConnected;
    }



    // All Account interactions here ///


    public boolean CreateAccount(Account account) {
        dbManager.InsertRecord("Accounts",account.getValues());
        return true;
    }

    public boolean ChangeAccount(Account account, int id) {
        dbManager.UpdateRecord("Accounts",account.getValues(),id);
        return true;
    }

    public boolean DeleteAccount(int id) {
        dbManager.DeleteRecord("Accounts",id);
        return true;
    }

    public ArrayList<Account> UpdateAccountsArray() {

        ArrayList<Account> accounts = new ArrayList<Account>();
        Cursor cursor = dbManager.getRecords(AccountsContract.FeedEntry.TABLE_NAME, "1", "1", null);
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            Account t = new Account(cursor.getString(cursor.getColumnIndex(AccountsContract.columns[0])),
                    cursor.getString(cursor.getColumnIndex(AccountsContract.columns[1])),
                    cursor.getString(cursor.getColumnIndex(AccountsContract.columns[2])),
                    cursor.getString(cursor.getColumnIndex(AccountsContract.columns[3])),
                    cursor.getString(cursor.getColumnIndex(AccountsContract.columns[4]))
            );
            t.setID(cursor.getInt(cursor.getColumnIndex(AccountsContract.FeedEntry._ID)));
            accounts.add(t);
        }
        return accounts;
    }


    // All Settings interactions here ///

    public boolean ChangeSettings(Properties settings) {
        String[] values=new String[5];
        String str;

        Set properties = settings.keySet();   // get set-view of keys
        Iterator itr = properties.iterator();

        int i=0;


        while(itr.hasNext()) {
            str = (String) itr.next();
            values[0] = "";
            values[1] = str;
            values[2] = settings.getProperty(str);
            values[3] = "";
            values[4] = "";
            long s = dbManager.UpdateRecord("Settings", values, i+1);
            Log.i("Printing msg" + i  + "","The setting of " + str + " is " + settings.getProperty(str) + ". ID changed: "+s);
            i++;
        }

        return true;
    }

    public Cursor ListSettings() {
        return dbManager.getRecords("Settings", "1", "1", null);
    }

    public boolean RevertDefaultSettings() {
        int i=0;
        for (i = 0; i < SettingsContract.DefaultSettings.length; i++) {
            String[] values_arr = {SettingsContract.DefaultSettings[i][0], SettingsContract.DefaultSettings[i][1], SettingsContract.DefaultSettings[i][2],SettingsContract.DefaultSettings[i][3]};
            long s = dbManager.UpdateRecord("Settings", values_arr, i+1);
            Log.i("Settings updated",""+s);
            if (s == -1)
                return false;
            if (s==0)
            {
                dbManager.InsertRecord("Settings",values_arr);
            }
        }
        while(i<dbManager.GetAmountOfRecords("Settings"))
        {
            dbManager.DeleteRecord("Settings",i+1);
        }
        return true;
    }


        ///////////////////////////////////////
       ///////////////////////////////////////
      // All Providers interactions here ////
     ///////////////////////////////////////
    ///////////////////////////////////////

    public String[] UpdateProvidersURIArray() {

        ArrayList<Provider> providers = this.UpdateProvidersArray();
        Log.i("creating",""+providers.size());
        String[] URIs = new String[providers.size()];
        for(int i=0;i<providers.size();i++)
        {
            Log.i("creating",""+providers.get(i).getProvider_uri());
            URIs[i] = providers.get(i).getProvider_uri();
        }
        return URIs;
    }

    public ArrayList<Provider> UpdateProvidersArray() {

        ArrayList<Provider> providers = new ArrayList<Provider>();
        Cursor cursor = dbManager.getRecords("Providers", "1", "1", "ORDER By "+ProvidersContract.FeedEntry._ID + " ASC");
        if (cursor.getCount() == 0)
            return providers;
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            providers.add(new Provider(
                    cursor.getInt(cursor.getColumnIndex(ProvidersContract.FeedEntry._ID)),
                    cursor.getString(cursor.getColumnIndex(ProvidersContract.columns[0])),
                    cursor.getString(cursor.getColumnIndex(ProvidersContract.columns[1])),
                    cursor.getString(cursor.getColumnIndex(ProvidersContract.columns[2]))
            ));
        }
        return providers;
    }

    public boolean CreateProvider(Provider provider) {
        // Add new provider
        String[] values_provider = new String[3];
        values_provider[0] = provider.getProvider_name();
        values_provider[1] = provider.getProvider_uri();
        values_provider[2] = provider.getHash_strength();
        if (dbManager.InsertRecord("Providers", values_provider) != (-1))
            return true;
        else
            return false;
    }

    public boolean ChangeProvider(Provider provider, int id) {
        String[] values_provider = new String[3];
        values_provider[0] = provider.getProvider_name();
        values_provider[1] = provider.getProvider_uri();
        values_provider[2] = provider.getHash_strength();
        if (dbManager.UpdateRecord("Providers", values_provider, id) != (-1))
            return true;
        else
            return false;
    }

    public boolean DeleteProvider(int id) {
        if (dbManager.DeleteRecord("Providers", id) == 1)
            return true;
        else
            return false;
    }



    //// All Remote Data interactions here




}
