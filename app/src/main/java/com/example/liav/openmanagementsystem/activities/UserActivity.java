package com.example.liav.openmanagementsystem.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.App;
import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments.NotesFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments.ProfileFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments.ScheduleFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments.TimeTableFragment;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.services.MainService;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;
import com.example.liav.openmanagementsystem.utils.UserDataManager;

public class UserActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private boolean exit = false;
    private UserDataManager dataManager;
    private DrawerLayout drawer;

    public UserDataManager getDataManager()
    {
        return this.dataManager;
    }

    private TimeTableFragment timeTableFragment;
    private ScheduleFragment scheduleFragment;
    private NotesFragment notesFragment;
    private ProfileFragment profileFragment;

    private void initializeDataManager(Account account)
    {
        dataManager = new UserDataManager(this.getApplicationContext());
        UserDataHolder.getInstance().setData(dataManager);
        dataManager.SetAccount(account);

    }

    private void initializeLayout()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        drawer = findViewById(R.id.draw_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        TextView navID = (TextView) headerView.findViewById(R.id.userID);
        navID.setText(this.dataManager.getAccountSession().getUsername() + "@" + this.dataManager.getAccountSession().getProvider_uri());


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.open_drawer,R.string.close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setDefaults()
    {
        //drawer.openDrawer(Gravity.LEFT,true);

        //drawer.openDrawer(GravityCompat.START);
        this.setTitle("Profile");
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, this.profileFragment).commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        Intent intent = getIntent();

        Account user = null;
        if(intent.getStringExtra("provider_uri").equals(Account.LOCAL_PROVIDER))
        {
            user = new Account(
                    intent.getStringExtra("user"),
                    "",
                    intent.getStringExtra("email"),
                    intent.getStringExtra("provider_uri"),
                    intent.getStringExtra("provider_hash")
            );
        }
        else
        {
            user = new Account(
                    intent.getStringExtra("user"),
                    "",
                    intent.getStringExtra("email"),
                    intent.getStringExtra("provider_uri"),
                    intent.getStringExtra("provider_hash"),
                    intent.getStringExtra("token"),
                    intent.getIntExtra("permission_role",3)
            );
        }


        Intent serviceIntent2 = new Intent(this, MainService.class);
        if(user.getProvider_uri().equals(Account.LOCAL_PROVIDER))
        {
            serviceIntent2.putExtra("connectionType","local");
        }
        else
        {
            serviceIntent2.putExtra("connectionType","remote");
            serviceIntent2.putExtra("user",user.getUsername());
            serviceIntent2.putExtra("provider",user.getProvider_uri());
        }
        startService(serviceIntent2);


        this.timeTableFragment = new TimeTableFragment();
        this.timeTableFragment.setDataManager(this.dataManager);

        this.scheduleFragment = new ScheduleFragment();

        this.notesFragment =  new NotesFragment();
        this.notesFragment.setContext(getApplicationContext());


        this.profileFragment = new ProfileFragment();

        if(user.getProvider_uri().equals(Account.LOCAL_PROVIDER))
        {
            this.profileFragment.setValuesLocal();
        }
        else
        {
            this.profileFragment.setValues(
                    intent.getStringExtra("user"),
                    intent.getStringExtra("email"),
                    intent.getStringExtra("provider_uri"),
                    intent.getStringExtra("token"));
        }


        initializeDataManager(user);

        initializeLayout();

        setDefaults();
    }

    @Override
    protected void onDestroy() {

        this.dataManager = null;
        UserDataHolder.getInstance().setData(null);

        super.onDestroy();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        this.timeTableFragment.updateCurrentTab();
        this.notesFragment.updateFragment();
        this.scheduleFragment.updateCurrentTab();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId())
        {
            case R.id.nav_profile:
                this.setTitle("Profile");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,this.profileFragment).commit();
                break;
            case R.id.nav_timetable:
                this.setTitle("Time Table");
                //TimeTableFragment timeTableFragment = this.timeTableFragment;
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,this.timeTableFragment).commit();
                break;
            case R.id.nav_schedule:
                this.setTitle("Schedule & Calendar");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, this.scheduleFragment).commit();
                break;
            case R.id.nav_notes:
                this.setTitle("Notes");
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, this.notesFragment).commit();
                break;

            case R.id.nav_logout:
                exit = true;
                onBackPressed();
                break;

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
    @Override
    public void onBackPressed() {
        //exitActivity();
        if (drawer.isDrawerOpen(GravityCompat.START) && exit == false) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
/*

TODO: Check SQLITE For allowing confirm button or exiting without it!

 */
            if (AppDataHolder.getInstance().getData().UpdateSettings().getProperty("warn_before_exiting_local_session").equals("true")) {
                new AlertDialog.Builder(this)
                        .setTitle("Exit?")
                        .setMessage("Are you completely sure you finished your homework?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                switch (whichButton) {
                                    case DialogInterface.BUTTON_POSITIVE:
                                        exitActivity();
                                        break;
                                    case DialogInterface.BUTTON_NEGATIVE:
                                        break;
                                }

                            }
                        })
                        .setNegativeButton("No", null).show();
            } else {
                exitActivity();
            }
        }
    }

    private void exitActivity()
    {
        Intent serviceIntent = new Intent(this, MainService.class);
        stopService(serviceIntent);
        dataManager.LogoutUser();
        this.finish();
        super.onBackPressed();


    }
    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
