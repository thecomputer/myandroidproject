package com.example.liav.openmanagementsystem.utils.layout;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;


import com.example.liav.openmanagementsystem.activities.ItemActivity;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.utils.dataholders.FormDataHolder;

import java.util.ArrayList;

import static com.example.liav.openmanagementsystem.utils.layout.DialogBuilder.setDialogNoteList;
import static com.example.liav.openmanagementsystem.utils.layout.ListBuilder.buildNoteList;


public class NotesListBuilder {

    private NotesListBuilder() {}

    public static void buildNotesEvents(Note note, LinearLayout layout2, Context context)
    {

            Log.i("logging building",""+note.getId());
            ListBuilder.addLayoutNoteList(layout2);
            setListener(layout2,note.getId(),context,note.getTitle(),note);
            buildListItem(layout2,context,note);
    }

    private static void buildListItem(LinearLayout layout2,Context context,Note note)
    {
        buildNoteList(
                layout2,
                context,
                note.getTitle(),
                note.getDesc()
        );
    }
    private static void setListener(LinearLayout linearLayout, int i, Context context, String title,final Note note)
    {
        final Context ClassContext = context;
        final int layoutID = i;
        final String t = title;
        linearLayout.setOnLongClickListener(new View.OnLongClickListener()
        {

            @Override
            public boolean onLongClick(View v) {
                String[] options = { "Edit note", "Delete note" };
                FormDataHolder.getInstance().getData().note = note;
                setDialogNoteList(options, ClassContext,layoutID,t);
                return true;

            }

        });

        linearLayout.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {

                Intent myIntent;
                myIntent = new Intent(ClassContext, ItemActivity.class);
                myIntent.putExtra("type", "note");

                myIntent.putExtra("title",note.getTitle());
                myIntent.putExtra("description",note.getDesc());

                ClassContext.startActivity(myIntent);
            }

        });


    }


}