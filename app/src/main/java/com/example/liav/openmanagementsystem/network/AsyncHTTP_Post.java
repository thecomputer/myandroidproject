package com.example.liav.openmanagementsystem.network;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.CalendarEventsListFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.TimetableEventsListFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments.NotesFragment;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;
import com.example.liav.openmanagementsystem.models.user_templates.NotesContract;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.CalendarContract;
import com.example.liav.openmanagementsystem.models.user_templates.timetableContract.TimetableContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class AsyncHTTP_Post extends AsyncTask<Void, Void, Integer> {
    public static final int TIMETABLE_TYPE = 1;
    public static final int NOTES_TYPE = 2;
    public static final int CALENDAR_TYPE = 3;

    public static final String REQUEST_METHOD = "POST";
    public static final int READ_TIMEOUT = 20000;
    public static final String charset = "UTF-8";
    public static final int CONNECTION_TIMEOUT = 20000;

    private int type;
    private String url;
    private HashMap<String,String> values;

    public AsyncHTTP_Post(HashMap<String,String> values, int type, String url) {
        this.type = type;
        this.url = url;
        this.values = values;
    }

    protected void onPreExecute()
    {

    }


    @Override
    protected Integer doInBackground(Void... voids)
    {



        //if(type == 1)
        HttpURLConnection connection = null;
        InputStream stream = null;
        try {

            URL url = new URL(this.url);

            // Append parameters to URL
            Uri.Builder builder = null;
            if(this.type == TIMETABLE_TYPE)
            {
                builder = new Uri.Builder()
                        .appendQueryParameter("title", this.values.get(TimetableContract.FeedEntry.COLUMN4_NAME_TITLE))
                        .appendQueryParameter("notes", this.values.get(TimetableContract.FeedEntry.COLUMN6_NAME_TITLE))
                        .appendQueryParameter("day", this.values.get(TimetableContract.FeedEntry.COLUMN1_NAME_TITLE))
                        .appendQueryParameter("minute", this.values.get(TimetableContract.FeedEntry.COLUMN3_NAME_TITLE))
                        .appendQueryParameter("hour", this.values.get(TimetableContract.FeedEntry.COLUMN2_NAME_TITLE))
                        .appendQueryParameter("desc", this.values.get(TimetableContract.FeedEntry.COLUMN5_NAME_TITLE));
            }
            if(this.type == CALENDAR_TYPE)
            {
                builder = new Uri.Builder()
                        .appendQueryParameter("title", this.values.get(CalendarContract.FeedEntry.COLUMN3_NAME_TITLE))
                        .appendQueryParameter("notes", this.values.get(CalendarContract.FeedEntry.COLUMN5_NAME_TITLE))
                        .appendQueryParameter("timestamp", this.values.get(CalendarContract.FeedEntry.COLUMN1_NAME_TITLE))
                        .appendQueryParameter("desc", this.values.get(CalendarContract.FeedEntry.COLUMN4_NAME_TITLE));
            }
            if(this.type == NOTES_TYPE)
            {
                builder = new Uri.Builder()
                        .appendQueryParameter("title", this.values.get(NotesContract.FeedEntry.COLUMN2_NAME_TITLE))
                        .appendQueryParameter("desc", this.values.get(NotesContract.FeedEntry.COLUMN3_NAME_TITLE));
            }
            String query = builder.build().getEncodedQuery();

            byte[] postData       = query.getBytes(StandardCharsets.UTF_8);
            int    postDataLength = postData.length;
            Log.i("query HTTP",query);

            connection = (HttpURLConnection) url.openConnection();
            //connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setRequestMethod("POST");
            connection.setFixedLengthStreamingMode(postDataLength);
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            connection.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
            connection.connect();
            connection.getOutputStream().write(postData);

            stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
            String result = reader.readLine();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return 0;
    }

    protected void onPostExecute(Integer intd)
    {

    }

}
