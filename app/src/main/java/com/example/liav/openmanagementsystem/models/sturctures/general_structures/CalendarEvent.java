package com.example.liav.openmanagementsystem.models.sturctures.general_structures;

public class CalendarEvent {

    private boolean notification_enabled;
    private String notes;
    private String title;
    private String description;
    private long timestamp;

    private int id;

    public CalendarEvent(int id, boolean notification_enabled, String title, String description, String notes, long timestamp) {
        this.notification_enabled = notification_enabled;
        this.notes = notes;
        this.title = title;
        this.description = description;
        this.timestamp = timestamp;
        this.id = id;
    }

    public boolean isNotification_enabled() {
        return notification_enabled;
    }

    public String getNotes() {
        return notes;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getID() {
        return id;
    }
}
