package com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments;



import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.CalendarEventsListFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.CalendarFragment;
import com.example.liav.openmanagementsystem.models.templates.EventsListFragmentTypes;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ScheduleFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public TabLayout tabLayout;
    public static final int int_items = 3;

    private Fragment selectedFragment;
    private int selectedTab;

        public String getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Today Events";
                case 1:
                    return "Calendar";
                case 2:
                    return "All Events";
            }
            return null;
        }

    private void releaseTab(int i)
    {
        this.selectedFragment.onDestroy();
    }

    private Fragment GetFragment(int position)
    {
        switch (position) {
            case 0:
                CalendarEventsListFragment fragment = new CalendarEventsListFragment();
                int[] params = {0,EventsListFragmentTypes.CalendarEventsToday.DEFAULT_ORDER};
                fragment.setType(EventsListFragmentTypes.CalendarEventsToday._CLASS,params);
                return fragment;
            case 1:
                return new CalendarFragment();
            case 2:
                CalendarEventsListFragment fragment2 = new CalendarEventsListFragment();
                fragment2.setType(EventsListFragmentTypes.CalendarEventsAll._CLASS,null);
                return fragment2;
        }
        return null;
    }

    private void changeTab(int position)
    {

        Fragment fragment = GetFragment(position);

        this.selectedFragment = fragment;
        this.selectedTab = position;


        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager == null)
        {

            fragmentManager = ((AppCompatActivity)getActivity()).getSupportFragmentManager();
            if(fragmentManager == null)
            {
                fragmentManager = getChildFragmentManager();
            }
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.viewPager,fragment);
        fragmentTransaction.commit();
    }

    public void updateCurrentTab()
    {
        if(this.selectedFragment != null)
        {
            if(this.selectedFragment instanceof CalendarEventsListFragment)
            {
                ((CalendarEventsListFragment)this.selectedFragment).updateExisting();
            }

        }
    }

    private void addTab(String title) {
        tabLayout.addTab(tabLayout.newTab().setText(title));
    }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            /**
             *Inflate tab_layout and setup Views.
             */
            View v = inflater.inflate(R.layout.fragment_general_tabbed, container, false);
            tabLayout = (TabLayout) v.findViewById(R.id.tabs);
            //viewPager = (ViewPager) v.findViewById(R.id.viewpager);
            ((TabLayout) v.findViewById(R.id.tabs)).setTabMode(TabLayout.MODE_FIXED);


            for(int i=0;i<int_items;i++)
            {
                addTab(getPageTitle(i));
            }

            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    //do stuff here
                    changeTab(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    releaseTab(tab.getPosition());
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {
                    changeTab(tab.getPosition());
                }
            });

            changeTab(0);

            return v;

        }
    }