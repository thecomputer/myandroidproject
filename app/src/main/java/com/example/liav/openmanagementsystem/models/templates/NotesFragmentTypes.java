package com.example.liav.openmanagementsystem.models.templates;

public final class NotesFragmentTypes {
    private NotesFragmentTypes() {}

    public static final int timestamp_items = 7;
    public static final int category_items = 2;
    public static final int order_items = 2;

    public static final String[] getCategorySelectorValues()
    {
        String[] strings = {SelectorStrings._DATE_SELECTOR,SelectorStrings._TITLE_ABC_SELECTOR};
        return strings;
    }

    public static final String[] getOrderSelectorValues()
    {
        String[] strings = {SelectorStrings.DESC_ORDER,SelectorStrings.ASC_ORDER};
        return strings;
    }

    public static final String[] getTimeStampSelectorValues()
    {
        String[] strings = {
                TimestampsStrings.ALL,
                TimestampsStrings.TODAY,
                TimestampsStrings.LAST_WEEK,
                TimestampsStrings.LAST_MONTH,
                TimestampsStrings.LAST_6_MONTHS,
                TimestampsStrings.LAST_YEAR,
                TimestampsStrings.LAST_3_YEARS
        };
        return strings;
    }

    public final static class SelectNotes {
        public static final int _DATE_SELECTOR = 0x0;
        public final static class DATE {
            public static final int DESC_ORDER = 0x0;
            public static final int ASC_ORDER = 0x1;
        }

        public static final int _TITLE_SELECTOR = 0x1;
        public final static class TITLE_ABC {
            public static final int DESC_ORDER = 0x1;
            public static final int ASC_ORDER = 0x2;
        }

        public static final int DEFAULT_ORDER = DATE.DESC_ORDER ;
    }

    public final static class Timestamps {
        public static final int ALL = 0x0;
        public static final int TODAY = 0x1;
        public static final int LAST_WEEK = 0x2;
        public static final int LAST_MONTH = 0x3;
        public static final int LAST_6_MONTHS = 0x4;
        public static final int LAST_YEAR = 0x5;
        public static final int LAST_3_YEARS = 0x6;
    }

    public final static class TimestampsStrings {
        public static final String ALL = "All Notes";
        public static final String TODAY = "Last 24 hours";
        public static final String LAST_WEEK = "Last Week";
        public static final String LAST_MONTH = "Last Month";
        public static final String LAST_6_MONTHS = "Last 6 Months";
        public static final String LAST_YEAR = "Last Year";
        public static final String LAST_3_YEARS = "Last 3 Years";
    }

    public final static class SelectorStrings {

        public static final String _DATE_SELECTOR = "Date";
        public static final String _TITLE_ABC_SELECTOR = "Title (ABC)";

        public static final String DESC_ORDER = "Descending";
        public static final String ASC_ORDER = "Ascending";

        public static final String DEFAULT_SELECTOR = _DATE_SELECTOR;
        public static final String DEFAULT_ORDER =  DESC_ORDER;
    }
}
