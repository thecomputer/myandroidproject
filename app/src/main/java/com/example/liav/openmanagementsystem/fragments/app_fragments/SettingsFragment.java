package com.example.liav.openmanagementsystem.fragments.app_fragments;

import android.os.Bundle;

import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.util.Log;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;

import java.util.Properties;

public class SettingsFragment extends PreferenceFragmentCompat {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setHasOptionsMenu(true);

        // Bind the summaries of EditText/List/Dialog/Ringtone preferences
        // to their values. When their values change, their summaries are
        // updated to reflect the new value, per the Android Design
        // guidelines.
        //bindPreferenceSummaryToValue(findPreference("example_text"));
        //bindPreferenceSummaryToValue(findPreference("example_list"));
    }

    private Properties properties;
    SwitchPreference switchPreference1;
    SwitchPreference switchPreference2;
    ListPreference listPreference;

    @Override
    public void onCreatePreferences(Bundle bundle, String s)
    {
        addPreferencesFromResource(R.xml.pref_general);
        ListPreference editPref =(ListPreference)findPreference("timezone_list");
        listPreference = editPref;
        Properties properties = AppDataHolder.getInstance().getData().UpdateSettings();
        this.properties = properties;
        editPref.setValueIndex(Integer.parseInt(properties.getProperty("timezone").toString()));

        SwitchPreference switch1 =(SwitchPreference) findPreference("enable_http_switch");
        SwitchPreference switch2 =(SwitchPreference) findPreference("enable_exit_switch");

        switchPreference1 = switch1;
        switchPreference2 = switch2;

        if(properties.getProperty("allow_insecure_connections").equals("false"))
            switch1.setChecked(false);
        else if(properties.getProperty("allow_insecure_connections").equals("true"))
            switch1.setChecked(true);

        if(properties.getProperty("warn_before_exiting_local_session").equals("false"))
            switch2.setChecked(false);
        else if(properties.getProperty("warn_before_exiting_local_session").equals("true"))
                switch2.setChecked(true);



        editPref.setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference,
                                                      Object newValue) {
                        ChangeSwitch(-1,newValue);
                        return true;
                    }
                });

        switch1.setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference,
                                                      Object newValue) {
                        Log.i("settings changed","0");
                        ChangeSwitch(0,newValue);
                        return true;
                    }
                });
        switch2.setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference,
                                                      Object newValue) {
                        Log.i("settings changed","1");
                        ChangeSwitch(1,newValue);
                        return true;
                    }
                });

    }

    /*public void ChangeTimezone(Object newValue)
    {
        this.properties.setProperty("timezone",newValue.toString());
        AppDataHolder.getInstance().getData().ChangeSettings(this.properties);
        this.properties = AppDataHolder.getInstance().getData().UpdateSettings();
    }*/
    Boolean switch1;
    Boolean switch2;
    public void ChangeSwitch(int i,Object newValue) {

        Log.i("settings", i + " " + newValue.toString());
        if (i == (-1)) {
            this.properties.setProperty("timezone", newValue.toString());
        }
        if (i == 0) {
            this.properties.setProperty("allow_insecure_connections", newValue.toString());
        }
        if (i == 1) {
            this.properties.setProperty("warn_before_exiting_local_session", newValue.toString());
        }

        AppDataHolder.getInstance().getData().ChangeSettings(this.properties);
    }
}
