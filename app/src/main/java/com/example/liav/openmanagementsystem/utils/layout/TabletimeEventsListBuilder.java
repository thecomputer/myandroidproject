package com.example.liav.openmanagementsystem.utils.layout;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.activities.ItemActivity;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;
import com.example.liav.openmanagementsystem.utils.dataholders.FormData;
import com.example.liav.openmanagementsystem.utils.dataholders.FormDataHolder;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.example.liav.openmanagementsystem.utils.layout.DialogBuilder.setDialogEventList;
import static com.example.liav.openmanagementsystem.utils.layout.ListBuilder.buildTimetableEventList;

public class TabletimeEventsListBuilder {
    private TabletimeEventsListBuilder() {}

    public static void buildTimetableEvents(TimetableEvent timetableEvent,LinearLayout layout, Context context)
    {

            Log.i("logging building",""+ timetableEvent.getID());
            ListBuilder.addLayoutItemList(layout);

            setListener(layout, timetableEvent.getID(),context, timetableEvent.getHour(), timetableEvent.getMinute(),timetableEvent.getDay(),timetableEvent);
            buildListItem(layout,context, timetableEvent);
    }

    private static void buildListItem(LinearLayout layout2,Context context,TimetableEvent timetableEvent)
    {

        buildTimetableEventList(
                layout2,
                //timetableEvent.getID(),
                context,
                timetableEvent.getHour(),
                timetableEvent.getMinute(),
                timetableEvent.getTitle(),
                timetableEvent.getDescription()
        );
    }
    private static void setListener(LinearLayout linearLayout, int i, Context context, int hour, int minute,int day,final TimetableEvent timetableEvent)
    {
        final Context ClassContext = context;
        final int layoutID = i;
        final int h = hour;
        final int m = minute;
        final int d = day;
        linearLayout.setOnLongClickListener(new View.OnLongClickListener()
        {

            @Override
            public boolean onLongClick(View v) {
                String[] options = { "Edit event", "Delete event" };
                FormDataHolder.getInstance().getData().timetableEvent = timetableEvent;
                setDialogEventList(options, ClassContext,layoutID,h,m);
                return true;

            }

        });

        linearLayout.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {

                Intent myIntent;
                myIntent = new Intent(ClassContext, ItemActivity.class);
                myIntent.putExtra("type", "event");
                SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm" );
                Calendar calendar = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
                calendar.set(Calendar.HOUR_OF_DAY,h);
                calendar.set(Calendar.MINUTE,m);
                String day = getDayTitle(d);
                String time = sdf.format(calendar.getTimeInMillis());
                String full_time = day + " " + time;

                myIntent.putExtra("title",timetableEvent.getTitle());
                myIntent.putExtra("description",timetableEvent.getDescription());
                myIntent.putExtra("time",full_time);
                myIntent.putExtra("notes",timetableEvent.getNotes());

                ClassContext.startActivity(myIntent);
            }

        });


    }
    public static String getDayTitle(int position) {

        switch (position) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
        }
        return "";
    }


}
