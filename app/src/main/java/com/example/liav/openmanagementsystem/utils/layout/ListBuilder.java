package com.example.liav.openmanagementsystem.utils.layout;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.R;

import java.text.DecimalFormat;

public class ListBuilder {
    private ListBuilder() {}

    public static void addLayoutItemList(LinearLayout layout2)
    {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(6,6,6,6);
        layout2.setLayoutParams(params);
        layout2.setOrientation(LinearLayout.VERTICAL);
        layout2.setBackgroundResource(R.drawable.customborder);

    }

    public static void addLayoutNoteList(LinearLayout layout2)
    {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(5,7,5,7);
        layout2.setLayoutParams(params);
        layout2.setOrientation(LinearLayout.VERTICAL);
        layout2.setBackgroundResource(R.drawable.roundedborder);

    }

    public static void buildCalendarEventList(LinearLayout layout2, Context context,int hour, int minute,int year,int month, int day,String title,String desc)
    {
        TextView rowTextView;
        rowTextView = layout2.findViewById(R.id.main_date);

        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,17);

        DecimalFormat formatter = new DecimalFormat("00");
        String Minute = formatter.format(minute);
        String Hour = formatter.format(hour);

        SpannableString spanString = new SpannableString(day+"."+(month+1)+"."+year+" - " + Hour + ":" + Minute);

        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        rowTextView.setText(spanString);

        //
        if(title != null) {
            rowTextView = layout2.findViewById(R.id.main_title);
            rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM, 16);
            spanString = new SpannableString(title);

            spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
            rowTextView.setText(spanString);
        }
        //
        if(desc != null) {
            rowTextView = layout2.findViewById(R.id.main_desc);
            rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM, 15);
            spanString = new SpannableString(desc);

            spanString.setSpan(new StyleSpan(Typeface.NORMAL), 0, spanString.length(), 0);
            rowTextView.setText(spanString);
        }
    }

    public static void buildTimetableEventList(LinearLayout layout2, Context context,int hour, int minute,String title,String desc)
    {
        TextView rowTextView;
        rowTextView = layout2.findViewById(R.id.main_title);

        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,17);

        DecimalFormat formatter = new DecimalFormat("00");
        String Minute = formatter.format(minute);

        SpannableString spanString;
        spanString = new SpannableString(hour + ":" + Minute);

        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        rowTextView.setText(spanString);

        if(title != null)
        {
            rowTextView = layout2.findViewById(R.id.main_desc);
            rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,16);
            spanString = new SpannableString(title);

            spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
            rowTextView.setText(spanString);
        }


        if(desc != null) {
            rowTextView = layout2.findViewById(R.id.main_notes);
            rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM, 15);
            spanString = new SpannableString(desc);

            spanString.setSpan(new StyleSpan(Typeface.NORMAL), 0, spanString.length(), 0);
            rowTextView.setText(spanString);
        }
    }

    public static void buildNoteList(LinearLayout layout2, Context context,String title,String desc)
    {
        TextView rowTextView;
        rowTextView = layout2.findViewById(R.id.main_title);

        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,17);


        SpannableString spanString;
        if(title != null)
        {
            spanString = new SpannableString(title);

            spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
            rowTextView.setText(spanString);
        }

        //
        if(desc != null)
        {
            rowTextView = layout2.findViewById(R.id.main_desc);
            rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,13);
            spanString = new SpannableString(desc);

            spanString.setSpan(new StyleSpan(Typeface.NORMAL), 0, spanString.length(), 0);
            rowTextView.setText(spanString);
        }

        //
        /*rowTextView = new TextView(context);
        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,15);
        spanString = new SpannableString(desc);

        spanString.setSpan(new StyleSpan(Typeface.NORMAL), 0, spanString.length(), 0);
        rowTextView.setText(spanString);
        rowTextView = new TextView(context);
        rowTextView.setText(spanString);
        layout2.addView(rowTextView);*/
    }
}
