//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.example.liav.openmanagementsystem.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.broadcastreceivers.NetworkChangeReceiver;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Provider;
import com.example.liav.openmanagementsystem.models.templates.InputActivityTypes;
import com.example.liav.openmanagementsystem.network.AsyncHTTP_Login;
import com.example.liav.openmanagementsystem.network.HTTPHelper;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;
import com.example.liav.openmanagementsystem.utils.AppDataManager;
import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.utils.dataholders.FormData;
import com.example.liav.openmanagementsystem.utils.dataholders.FormDataHolder;
import com.example.liav.openmanagementsystem.utils.layout.DialogBuilder;
import com.example.liav.openmanagementsystem.utils.timezone.GPSHelper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {

    NetworkChangeReceiver networkChangeReceiver = new NetworkChangeReceiver();

    Context context;
    private AppDataManager appDataManager;
    @Override
    public void onPause() {
        unregisterReceiver(networkChangeReceiver);
        Log.i("unregister","network");
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //android.os.Process.killProcess(android.os.Process.myPid());
    }

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        AppDataHolder.getInstance().setData(new AppDataManager(getApplicationContext()));
        FormDataHolder.getInstance().setData(new FormData());
        appDataManager = AppDataHolder.getInstance().getData();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        else
        {
            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            this.locationManager = locationManager;
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        }
        //LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        //Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        PrintAccounts();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
                    this.locationManager = locationManager;
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                    return;
                }
                // other 'case' lines to check for other
                // permissions this app might request.
            }
        }
    }

    public void onLocationChanged(Location location) {
        if (location != null) {
            GPSHelper.location = location;

            Log.v("Location Changed", location.getLatitude() + " and " + location.getLongitude());
            locationManager.removeUpdates(this);
            //this.locationManager = null;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    protected void onResume() {
        System.gc();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(networkChangeReceiver,filter);
        Log.i("register","network");
        if(AppDataHolder.getInstance().getData() != null)
        {
            PrintAccounts();
        }
        super.onResume();
    }



    public void doLocalLogin()
    {
        //dataManager.LoginLocalUser();
        Intent myIntent = new Intent(this, UserActivity.class);
        myIntent.putExtra("user","local");
        myIntent.putExtra("password","");
        myIntent.putExtra("provider_uri", Account.LOCAL_PROVIDER);
        myIntent.putExtra("email","");
        myIntent.putExtra("provider_hash","");
        startActivity(myIntent);
    }

    public void doRemoteLogin(Account account)
    {
        HTTPHelper.LoginHTTP(account,account.getProvider_uri(),account.getProvider_uri()+"login.php",this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Intent myIntent;
        switch (item.getItemId()) {

            case R.id.actionabout:
                myIntent = new Intent(this, AboutActivity.class);
                startActivity(myIntent);
                return true;
            case R.id.actionproviders:
                myIntent = new Intent(this, ProvidersActivity.class);
                startActivity(myIntent);
                return true;

            case R.id.actionsetting:
                myIntent = new Intent(this, SettingsActivity.class);
                startActivity(myIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu1, menu);
        return true;
    }

    /////////////////////////////////

    private ArrayList<Account> LoadAccounts()
    {
        ArrayList<Account> accounts = appDataManager.UpdateAccountsArray();
        return accounts;
    }

    private void setListener(LinearLayout linearLayout, int i, Account account)
    {
        final Context ClassContext = this.context;
        final int layoutID = i;
        final Account account1 = account;
        linearLayout.setOnLongClickListener(new View.OnLongClickListener()
        {

            @Override
            public boolean onLongClick(View v) {
                FormDataHolder.getInstance().getData().account = account1;
                String[] options = { "Edit account", "Delete account" };
                DialogBuilder.setDialogAccountList(options, ClassContext, layoutID);
                return true;


            }


        });
        linearLayout.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                doRemoteLogin(account1);
            }
        });

    }

    private void setLocalListener(LinearLayout linearLayout, int i)
    {
        //final Context ClassContext = getApplicationContext();
        final int layoutID = i;
        final Context context = this.context;
        linearLayout.setOnLongClickListener(new View.OnLongClickListener()
        {

            @Override
            public boolean onLongClick(View v) {
                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                alertDialog.setTitle("OOPS");
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.setMessage("You can't modify the default (local) user...");
                alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                alertDialog.show();
                return true;


            }


        });
        linearLayout.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                doLocalLogin();
            }
        });

    }

    private void buildListItem(LinearLayout layout2, ArrayList<Account> accounts, int i)
    {
        TextView rowTextView;
        rowTextView = new TextView(getApplicationContext());

        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,17);

        SpannableString spanString;// = new SpannableString(accounts.get(i).getEmail());

        //spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        //rowTextView.setText(spanString);

        //layout2.addView(rowTextView);
//
        rowTextView = new TextView(getApplicationContext());
        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,15);
        spanString = new SpannableString(accounts.get(i).getUsername());

        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        rowTextView.setText(spanString);
        rowTextView = new TextView(getApplicationContext());
        rowTextView.setText(spanString);
        layout2.addView(rowTextView);


        //
        rowTextView = new TextView(getApplicationContext());
        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,16);
        spanString = new SpannableString(accounts.get(i).getProvider_uri());

        spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);
        rowTextView.setText(spanString);
        rowTextView = new TextView(getApplicationContext());
        rowTextView.setText(spanString);
        layout2.addView(rowTextView);
    }

    private void setListenerFloatingActionButton()
    {
        FloatingActionButton button = findViewById(R.id.fab);
        final Context context = this.context;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent;
                myIntent = new Intent(context, InputActivity.class);
                myIntent.putExtra("type", InputActivityTypes.CLASS_A_CREATE._NAME);
                myIntent.putExtra("subtype", InputActivityTypes.CLASS_A_CREATE.USER_REGISTER);
                startActivity(myIntent);
            }
        });
    }

    private void addLayoutItemList(LinearLayout layout2)
    {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(5,7,5,7);
        layout2.setLayoutParams(params);
        layout2.setOrientation(LinearLayout.VERTICAL);
        layout2.setBackgroundResource(R.drawable.customborder);

    }

    private void PrintAccounts()
    {

        // list all events!
        LinearLayout eventsLayout = findViewById(R.id.accountsLayout);
        if(((LinearLayout) eventsLayout).getChildCount() > 0)
            ((LinearLayout) eventsLayout).removeAllViews();

        ArrayList<Account> accounts = LoadAccounts();
        Log.i("something",""+accounts.size());
        for(int i=0;i<accounts.size();i++)
        {
            LinearLayout layout2 = new LinearLayout(this);
            addLayoutItemList(layout2);
            eventsLayout.addView(layout2);
            buildListItem(layout2,accounts,i);
            if(i != 0)
                setListener(layout2,i,accounts.get(i));
            else
                setLocalListener(layout2,i);

        }
        setListenerFloatingActionButton();
    }

    AlertDialog actions;

    DialogInterface.OnClickListener actionListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            //Intent myIntent;
            switch (which) {
                case 0: // Add new events

                    break;
                case 1: // See events

                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {

    }

    ////////////////////////////
}
