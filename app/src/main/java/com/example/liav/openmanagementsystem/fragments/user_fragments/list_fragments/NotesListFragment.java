package com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.utils.layout.NotesListBuilder;
import com.example.liav.openmanagementsystem.utils.recyclerview_adapters.CalendarEventsAdapter;
import com.example.liav.openmanagementsystem.utils.recyclerview_adapters.NotesListAdapter;

import java.util.ArrayList;

public class NotesListFragment extends Fragment {

    private boolean hasLoadedOnce = false;
    private int day;
    private Context context;
    private boolean eventsEmpty = true;


    private View inf;
    //private int[] params;
    private ArrayList<Note> notes;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public void setType(ArrayList<Note> notes,Context context)
    {
        //this.params = params;
        this.notes = notes;
        this.context = context;
    }
    public NotesListFragment()
    {
        super();

    }

    @Override
    public void onDetach() {
        this.context = null;
        super.onDetach();
    }



    @Override
    public void onAttach(Context context) {
        this.context = context;

        super.onAttach(context);

    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible) {
        super.setUserVisibleHint(true);

        if (this.isVisible()) {
            if (isFragmentVisible && hasLoadedOnce) {
                // start update code here so this code run only when user select this fragment
                //this.updateExisting();
            }
        }
    }


    private void setRecyclerViewList(View inf)
    {
        recyclerView = (RecyclerView) inf.findViewById(R.id.notesList);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(false);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this.context) {
            @Override
            public boolean canScrollVertically()
            {
                return false;
            }
        };

        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new NotesListAdapter(this.notes);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View inf = inflater.inflate(R.layout.fragment_notes_list, container, false);
        this.inf = inf;
        Log.i("notes",""+this.notes.size());
        setRecyclerViewList(inf);

        this.hasLoadedOnce = true;
        return inf;

    }



}
