package com.example.liav.openmanagementsystem.models.user_templates;

import android.content.ContentValues;
import android.provider.BaseColumns;

public final class NotesContract {

    private NotesContract() {}

    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "Notes";
        public static final String COLUMN1_NAME_TITLE = "unix_timestamp";
        public static final String COLUMN2_NAME_TITLE = "note_title";
        public static final String COLUMN3_NAME_TITLE= "note_description";
        //public static final String COLUMN4_NAME_TITLE= "linked_media_count";
    }

    public static final String[] columns = {FeedEntry.COLUMN1_NAME_TITLE,FeedEntry.COLUMN2_NAME_TITLE,FeedEntry.COLUMN3_NAME_TITLE};

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN1_NAME_TITLE + " INTEGER," + // unix timestamp
                    FeedEntry.COLUMN2_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN3_NAME_TITLE + " TEXT);";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    public static ContentValues PrepareWriteToTable(String[] values_arr)
    {
        if(values_arr.length != 3)
            return null;
// Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedEntry.COLUMN1_NAME_TITLE, values_arr[0]);
        values.put(FeedEntry.COLUMN2_NAME_TITLE, values_arr[1]);
        values.put(FeedEntry.COLUMN3_NAME_TITLE, values_arr[2]);
        //values.put(FeedEntry.COLUMN4_NAME_TITLE, values_arr[3]);
// Insert the new row, returning the primary key value of the new row
        return values;
    }
}
