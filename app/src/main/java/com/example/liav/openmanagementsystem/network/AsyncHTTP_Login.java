package com.example.liav.openmanagementsystem.network;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.example.liav.openmanagementsystem.activities.ItemActivity;
import com.example.liav.openmanagementsystem.activities.UserActivity;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

public class AsyncHTTP_Login extends AsyncTask<Void, Void, Integer>
{
    public static final int TIMETABLE_TYPE = 1;
    public static final int NOTES_TYPE = 2;
    public static final int CALENDAR_TYPE = 3;

    private String password;
    private String username;
    private String url;
    private String uri_provider;

    private Context context;

    ////

    private int errorCode;

    private boolean successLogin = false;
    private String email;
    private String token;
    private int permission_role;


    public AsyncHTTP_Login(String url,String uri_provider, String username, String password, Context context) {
        this.context = context;
        this.username = username;
        this.password = password;
        this.url = url;
        this.uri_provider = uri_provider;
    }

    protected void onPreExecute()
    {

    }


    @Override
    protected Integer doInBackground(Void... voids)
    {
        //if(type == 1)
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(this.url);
            connection = (HttpURLConnection) url.openConnection();

            if(
                    (connection instanceof HttpURLConnection && AppDataHolder.getInstance().getData().UpdateSettings().getProperty("allow_insecure_connections").equals("true")) ||
                            (connection instanceof HttpsURLConnection)
            )
            {
                connection.connect();

                // Append parameters to URL
                Uri.Builder builder;

                builder = new Uri.Builder()
                        .appendQueryParameter("username", this.username)
                        .appendQueryParameter("password",this.password);

                String query = builder.build().getEncodedQuery();

                byte[] postData       = query.getBytes(StandardCharsets.UTF_8);
                int    postDataLength = postData.length;
                Log.i("query HTTP",query);

                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setFixedLengthStreamingMode(postDataLength);
                connection.setDoOutput(true);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                connection.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
                connection.connect();
                connection.getOutputStream().write(postData);

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = null;
                JSONObject jsonObject = null;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                    jsonObject = new JSONObject(line);
                    Log.i("Response: ", line);
                }


                String username = jsonObject.getString("username");
                String token = jsonObject.getString("token");
                int permission_role = jsonObject.getInt("permission_role");
                String email = jsonObject.getString("email");
                if(username != "" && token != "")
                {
                    this.successLogin = true;

                    this.email = email;
                    this.permission_role = permission_role;
                    this.token = token;
                }
                else
                {
                    this.successLogin = false;
                    this.errorCode = 1;
                }

            }
            else
            {
                this.successLogin = false;
                this.errorCode = 2;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        return 0;
    }
    protected void onPostExecute(Integer intd)
    {
        if(successLogin){
            Intent intent = new Intent(context, UserActivity.class);
            intent.putExtra("user",this.username);
            intent.putExtra("email",this.email);
            intent.putExtra("provider_uri",this.uri_provider);
            intent.putExtra("provider_hash","");
            intent.putExtra("token",this.token);
            intent.putExtra("permission_role",this.permission_role);
            context.startActivity(intent);
        }
        else
        {
            String error = null;
            if(this.errorCode == 1)
                error = "Failed login. Check your account credentials!";
            if(this.errorCode == 2)
                error = "Failed login. You are trying to connect over insecure connection. To allow that change it in the settings section!";
            else
                error = "Something wrong happened";

            AlertDialog alertDialog = new AlertDialog.Builder(this.context).create();
            alertDialog.setTitle("Failed to connect...");
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setMessage("ERROR: " + error);
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            alertDialog.show();
        }

    }

}