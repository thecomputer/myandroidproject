package com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.TimetableEventsListFragment;
import com.example.liav.openmanagementsystem.models.templates.EventsListFragmentTypes;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;
import com.example.liav.openmanagementsystem.utils.UserDataManager;

import java.util.Calendar;

public class TimeTableFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    private UserDataManager dataManager;

    public TabLayout tabLayout;
    public static final int int_items = 7;

    private TimetableEventsListFragment selectedFragment;
    private int selectedTab=0;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_general_tabbed, container, false);
        Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();

        initializeLayout(v);
        initializeTabs();

        selectTab(c.get(Calendar.DAY_OF_WEEK)-1);

        //changeTab(c.get(Calendar.DAY_OF_WEEK)-1);

        return v;
    }

    private void initializeLayout(View v)
    {
        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        ((TabLayout) v.findViewById(R.id.tabs)).setTabMode(TabLayout.MODE_SCROLLABLE);
    }

    private void selectTab(int position)
    {
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        tab.select();
    }


    private void initializeTabs()
    {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //do stuff here
                changeTab(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                releaseTab(tab.getPosition());
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                changeTab(tab.getPosition());
            }
        });
        for(int i=0;i<int_items;i++)
        {
            addTab(getPageTitle(i));
        }
    }

    public void updateCurrentTab()
    {
        if(this.selectedFragment != null)
        {
            this.selectedFragment.updateExisting();
        }
    }

    private void releaseTab(int i)
    {
        this.selectedFragment.onDestroy();
    }

    public String getPageTitle(int position) {

        switch (position) {
            case 0:
                return "Sunday";
            case 1:
                return "Monday";
            case 2:
                return "Tuesday";
            case 3:
                return "Wednesday";
            case 4:
                return "Thursday";
            case 5:
                return "Friday";
            case 6:
                return "Saturday";
        }
        return null;
    }

    public void setDataManager(UserDataManager dataManager)
    {
        this.dataManager = dataManager;
    }

    private void changeTab(int position)
    {
        int[] params = new int[1];
        TimetableEventsListFragment fragment = new TimetableEventsListFragment();
        params[0] = position; // day of fragment!
        //fragment.setDataManager(this.dataManager);
        fragment.setType(EventsListFragmentTypes.TimeTableEvents._CLASS,params);


        this.selectedFragment = fragment;
        this.selectedTab = position;

        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager == null)
        {

            fragmentManager = ((AppCompatActivity)getActivity()).getSupportFragmentManager();
            if(fragmentManager == null)
            {
                fragmentManager = getChildFragmentManager();
            }
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.viewPager,fragment);
        fragmentTransaction.commit();
    }

    //TabLayout.Tab[] days = new TabLayout.Tab[7];

    private void addTab(String title) {
        tabLayout.addTab(tabLayout.newTab().setText(title));
    }

}
