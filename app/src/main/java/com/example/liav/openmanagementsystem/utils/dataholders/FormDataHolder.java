package com.example.liav.openmanagementsystem.utils.dataholders;

import com.example.liav.openmanagementsystem.utils.UserDataManager;

public class FormDataHolder {
    private FormData data;
    public FormData getData() {return data;}
    public void setData(FormData data) {this.data = data;}

    private static final FormDataHolder holder = new FormDataHolder();
    public static FormDataHolder getInstance() {return holder;}
}
