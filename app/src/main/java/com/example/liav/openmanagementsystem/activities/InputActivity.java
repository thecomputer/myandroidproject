package com.example.liav.openmanagementsystem.activities;

import android.content.Intent;
import android.os.Bundle;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Provider;
import com.example.liav.openmanagementsystem.models.templates.InputActivityTypes;
import com.example.liav.openmanagementsystem.models.user_templates.NotesContract;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.CalendarContract;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;
import com.example.liav.openmanagementsystem.utils.dataholders.FormData;
import com.example.liav.openmanagementsystem.utils.dataholders.FormDataHolder;
import com.example.liav.openmanagementsystem.utils.layout.FormBuilder;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class InputActivity extends AppCompatActivity {


    //general settings

    private Button submitButton;
    private String type;
    private String subtype;
    private LinearLayout inputLayout;
    private LayoutInflater layoutInflater;

    private Intent intent;

    @Override
    public void onPause() {

        super.onPause();
        overridePendingTransition(0, 0);
    }


    private void createUser() {
        setTitle("Create a new User");
        FormBuilder.buildUserForm(inputLayout,layoutInflater.inflate(R.layout.form_register, null),null);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Account account = FormBuilder.getUserFormResults(getWindow().getDecorView().getRootView());
                AppDataHolder.getInstance().getData().CreateAccount(account);
                onBackPressed();
            }
        });
    }

    private void editUser() {
        setTitle("Edit a User");
        final Account account = FormDataHolder.getInstance().getData().account;

        FormBuilder.buildUserForm(inputLayout,layoutInflater.inflate(R.layout.form_register, null),account);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Account account2 = FormBuilder.getUserFormResults(getWindow().getDecorView().getRootView());
                AppDataHolder.getInstance().getData().ChangeAccount(account2,account.getID());
                onBackPressed();
            }
        });
    }

    private void deleteUser() {
        Account account = FormDataHolder.getInstance().getData().account;
        AppDataHolder.getInstance().getData().DeleteAccount(account.getID());
        onBackPressed();
    }

    private void createProvider() {
        setTitle("Create a new Provider");
        FormBuilder.buildProviderForm(inputLayout,layoutInflater.inflate(R.layout.form_provider, null),null);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Provider provider = FormBuilder.getProviderFormResults(getWindow().getDecorView().getRootView());
                AppDataHolder.getInstance().getData().CreateProvider(provider);
                onBackPressed();
            }
        });
    }

    private void editProvider() {
        setTitle("Edit a Provider");
        final int id = (int) intent.getIntExtra("id",0);
        final Provider provider = FormDataHolder.getInstance().getData().provider;

        FormBuilder.buildProviderForm(inputLayout,layoutInflater.inflate(R.layout.form_provider, null),provider);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                Provider provider2 = FormBuilder.getProviderFormResults(getWindow().getDecorView().getRootView());
                AppDataHolder.getInstance().getData().ChangeProvider(provider2,provider.getID());
                onBackPressed();
            }
        });
    }

    private void deleteProvider() {
        Provider provider = FormDataHolder.getInstance().getData().provider;
        AppDataHolder.getInstance().getData().DeleteProvider(provider.getID());
        onBackPressed();
    }

    ///////////////////////
    ///////////////////////


    private void createNotes() {
        setTitle("Create a new note");
        FormBuilder.buildNotesForm(inputLayout,layoutInflater.inflate(R.layout.form_notes, null),null);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                HashMap<String,String> values_map = FormBuilder.getNotesFormResults(getWindow().getDecorView().getRootView());
                Note note = new Note(-1,(int)TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()),values_map.get("note_title"),values_map.get("note_description"));
                UserDataHolder.getInstance().getData().createNotes(note);
                onBackPressed();
            }
        });
    }
    private void editNotes() {
        setTitle("Edit a note");
        final int id = (int) intent.getIntExtra("id",0);
        final Note note = FormDataHolder.getInstance().getData().note;

        FormBuilder.buildNotesForm(inputLayout,layoutInflater.inflate(R.layout.form_notes, null),note);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                HashMap<String,String> values_map = FormBuilder.getNotesFormResults(getWindow().getDecorView().getRootView());
                values_map.put(NotesContract.FeedEntry.COLUMN1_NAME_TITLE,""+note.getUnix_timestamp());
                UserDataHolder.getInstance().getData().editNote(id,values_map);
                onBackPressed();
            }
        });
    }

    private void createTimeTableEvent()
    {
        setTitle("Create a new timetable event");
        final int day = (int) intent.getIntExtra("day",0);
        FormBuilder.buildTimetableDateForm(inputLayout,getSupportFragmentManager(),layoutInflater.inflate(R.layout.form_timetable_event, null),null);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                HashMap<String,String> values = FormBuilder.getTimetableDateFormResults(getWindow().getDecorView().getRootView());
                values.put("day",""+day);
                UserDataHolder.getInstance().getData().createTimetableEvent(values);
                onBackPressed();
            }
        });
    }

    private void editTimeTableEvent()
    {
        setTitle("Edit a timetable event");
        final int id = (int) intent.getIntExtra("id",0);
        TimetableEvent timetableEvent = FormDataHolder.getInstance().getData().timetableEvent;
        final int day = timetableEvent.getDay();
        if(timetableEvent == null)
        {
            Log.i("event","null");
        }
        else
        {
            Log.i("event","not null");
        }
        FormBuilder.buildTimetableDateForm(inputLayout,getSupportFragmentManager(),layoutInflater.inflate(R.layout.form_timetable_event, null), timetableEvent);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                HashMap<String,String> values = FormBuilder.getTimetableDateFormResults(getWindow().getDecorView().getRootView());
                values.put("day",""+day);
                UserDataHolder.getInstance().getData().editTimetableEvent(id,values);
                onBackPressed();
            }
        });
    }

    private void createCalendarEvent()
    {
        setTitle("Create a new calendar event");
        FormBuilder.buildCalendarDateForm(inputLayout,getSupportFragmentManager(),layoutInflater.inflate(R.layout.form_calendar_event, null),null);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                HashMap<String,String> values = FormBuilder.getCalendarDateFormResults(getWindow().getDecorView().getRootView());
                values.put(CalendarContract.FeedEntry.COLUMN2_NAME_TITLE,"0");
                UserDataHolder.getInstance().getData().createCalendarEvent(values);
                onBackPressed();
            }
        });
    }

    private void editCalendarEvent()
    {
        setTitle("Edit a calendar event");
        final int id = (int) intent.getIntExtra("id",0);
        CalendarEvent calendarEvent = FormDataHolder.getInstance().getData().calendarEvent;
        FormBuilder.buildCalendarDateForm(inputLayout,getSupportFragmentManager(),layoutInflater.inflate(R.layout.form_calendar_event, null),calendarEvent);
        this.submitButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                HashMap<String,String> values = FormBuilder.getCalendarDateFormResults(getWindow().getDecorView().getRootView());
                values.put(CalendarContract.FeedEntry.COLUMN2_NAME_TITLE,"0");
                UserDataHolder.getInstance().getData().editCalendarEvent(id,values);
                onBackPressed();
            }
        });
    }

    ////////////////////////
    ////////////////////////


    private void ClassA_Create_Input()
    {
        switch(this.subtype)
        {
            case(InputActivityTypes.CLASS_A_CREATE.PROVIDER_REGISTER):
                createProvider();
                break;
            case(InputActivityTypes.CLASS_A_CREATE.USER_REGISTER):
                createUser();
                break;
        }
    }
    private void ClassA_Edit_Input()
    {
        switch(this.subtype)
        {
            case(InputActivityTypes.CLASS_A_EDIT.PROVIDER_EDIT):
                editProvider();
                break;
            case(InputActivityTypes.CLASS_A_EDIT.USER_EDIT):
                editUser();
                break;
        }
    }
    private void ClassA_Delete_Input()
    {
        switch(this.subtype)
        {
            case(InputActivityTypes.CLASS_A_DELETE.PROVIDER_DELETE):
                deleteProvider();
                break;
            case(InputActivityTypes.CLASS_A_DELETE.USER_DELETE):
                deleteUser();
                break;
        }
    }



    private void ClassB_Create_Input()
    {
        switch(this.subtype)
        {
            case(InputActivityTypes.CLASS_B_CREATE.TIMETABLE_EVENT):
                createTimeTableEvent();
                break;
            case(InputActivityTypes.CLASS_B_CREATE.NOTES_CREATE):
                createNotes();
                break;
            case(InputActivityTypes.CLASS_B_CREATE.CALENDAR_EVENT):
                createCalendarEvent();
                break;
        }
    }



    private void ClassB_Edit_Input()
    {
        switch(this.subtype)
        {
            case(InputActivityTypes.CLASS_B_EDIT.TIMETABLE_EVENT):
                editTimeTableEvent();
                break;
            case(InputActivityTypes.CLASS_B_EDIT.CALENDAR_EVENT):
                editCalendarEvent();
                break;
            case(InputActivityTypes.CLASS_B_EDIT.NOTES):
                editNotes();
                break;
        }
    }

    ///
    private void deleteTimeTableEvent()
    {
        final int id = (int) intent.getIntExtra("id",0);
        Log.i("delete_input", ""+id);
        // TODO: BUILD confirm dialog //
        UserDataHolder.getInstance().getData().deleteTimetableEvent(id);
        onBackPressed();
    }

    private void deleteCalendarEvent()
    {
        final int id = (int) intent.getIntExtra("id",0);
        Log.i("delete_input", ""+id);
        // TODO: BUILD confirm dialog //
        UserDataHolder.getInstance().getData().deleteCalendarEvent(id);
        onBackPressed();
    }
    private void deleteNote()
    {
        final int id = (int) intent.getIntExtra("id",0);
        Log.i("delete_input", ""+id);
        // TODO: BUILD confirm dialog //
        UserDataHolder.getInstance().getData().deleteNote(id);
        onBackPressed();
    }

    private void ClassB_Delete_Input()
    {
        switch(this.subtype)
        {
            case(InputActivityTypes.CLASS_B_DELETE.TIMETABLE_EVENT):
                deleteTimeTableEvent();
                break;
            case(InputActivityTypes.CLASS_B_DELETE.CALENDAR_EVENT):
                deleteCalendarEvent();
                break;
            case(InputActivityTypes.CLASS_B_DELETE.NOTES):
                deleteNote();
                break;
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        setTitle("Something more better");


        Intent intent = getIntent();
        this.intent = intent;
        String type = intent.getStringExtra("type");
        this.type = type;

        String subtype = intent.getStringExtra("subtype");
        this.subtype = subtype;

        this.submitButton = findViewById(R.id.submitButton);

        LinearLayout linearLayout = findViewById(R.id.inputLayout);
        this.inputLayout = linearLayout;
        this.layoutInflater = getLayoutInflater();

        switch(this.type)
        {

            case(InputActivityTypes.CLASS_A_CREATE._NAME):
                ClassA_Create_Input();
                break;
            case(InputActivityTypes.CLASS_B_CREATE._NAME):
                ClassB_Create_Input();
                break;
            case(InputActivityTypes.CLASS_B_EDIT._NAME):
                ClassB_Edit_Input();
                break;
            case(InputActivityTypes.CLASS_B_DELETE._NAME):
                ClassB_Delete_Input();
                break;
            case(InputActivityTypes.CLASS_A_EDIT._NAME):
                ClassA_Edit_Input();
                break;
            case(InputActivityTypes.CLASS_A_DELETE._NAME):
                ClassA_Delete_Input();
                break;


        }
    }


}
