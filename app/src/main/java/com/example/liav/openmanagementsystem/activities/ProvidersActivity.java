package com.example.liav.openmanagementsystem.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Provider;
import com.example.liav.openmanagementsystem.models.templates.InputActivityTypes;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;
import com.example.liav.openmanagementsystem.utils.dataholders.FormData;
import com.example.liav.openmanagementsystem.utils.dataholders.FormDataHolder;
import com.example.liav.openmanagementsystem.utils.layout.DialogBuilder;

import java.util.ArrayList;

public class ProvidersActivity extends AppCompatActivity {

    Context context;

    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }


    @Override
    protected void onResume() {
        super.onResume();
        LoadProviders();
        PrintProviders();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_providers);
        this.context = this;

        PrintProviders();
    }

    private ArrayList<Provider> LoadProviders()
    {
        ArrayList<Provider> providers = AppDataHolder.getInstance().getData().UpdateProvidersArray();
        return providers;
    }

    private void setListener(LinearLayout linearLayout, int i,final Provider provider)
    {
        //final Context ClassContext = getApplicationContext();
        final int layoutID = provider.getID();
        final Context context = this.context;
        linearLayout.setOnLongClickListener(new View.OnLongClickListener()
        {

            @Override
            public boolean onLongClick(View v) {
                FormDataHolder.getInstance().getData().provider = provider;
                String[] options = { "Edit provider", "Delete provider" };
                DialogBuilder.setDialogProviderList(options,context,layoutID);
                return true;


            }


        });
        linearLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) { }
        });

    }

    private void buildListItem(LinearLayout layout2, ArrayList<Provider> providers, int i)
    {
        TextView rowTextView;
        rowTextView = new TextView(getApplicationContext());

        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,17);


        SpannableString spanString = new SpannableString(providers.get(i).getProvider_name());

        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        rowTextView.setText(spanString);

        layout2.addView(rowTextView);

        //
        rowTextView = new TextView(getApplicationContext());
        rowTextView.setTextSize(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM,16);
        spanString = new SpannableString(providers.get(i).getProvider_uri());

        spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0);
        rowTextView.setText(spanString);
        rowTextView = new TextView(getApplicationContext());
        rowTextView.setText(spanString);
        layout2.addView(rowTextView);

    }

    private void setListenerFloatingActionButton()
    {
        FloatingActionButton button = findViewById(R.id.fab);
        final Context context = this.context;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent;
                myIntent = new Intent(context, InputActivity.class);
                myIntent.putExtra("type", InputActivityTypes.CLASS_A_CREATE._NAME);
                myIntent.putExtra("subtype", InputActivityTypes.CLASS_A_CREATE.PROVIDER_REGISTER);
                startActivity(myIntent);
            }
        });
    }

    private void addLayoutItemList(LinearLayout layout2)
    {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(5,7,5,7);
        layout2.setLayoutParams(params);
        layout2.setOrientation(LinearLayout.VERTICAL);
        layout2.setBackgroundResource(R.drawable.customborder);

    }

    private void PrintProviders()
    {

        // list all events!
        LinearLayout providersLayout = findViewById(R.id.providerLayout);
        if(((LinearLayout) providersLayout).getChildCount() > 0)
            ((LinearLayout) providersLayout).removeAllViews();

        ArrayList<Provider> providers = LoadProviders();
        Log.i("something",""+providers.size());
        for(int i=0;i<providers.size();i++)
        {
            LinearLayout layout2 = new LinearLayout(this);
            addLayoutItemList(layout2);
            providersLayout.addView(layout2);
            buildListItem(layout2,providers,i);
            setListener(layout2,i,providers.get(i));

        }
        setListenerFloatingActionButton();
    }

    AlertDialog actions;

    DialogInterface.OnClickListener actionListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            //Intent myIntent;
            switch (which) {
                case 0: // Edit provider

                    break;
                case 1: // Delete provider

                    break;
                default:
                    break;
            }
        }
    };
}
