package com.example.liav.openmanagementsystem.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.text.SpannableString;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.App;
import com.example.liav.openmanagementsystem.activities.UserActivity;


public class MainService extends Service {

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String connectionType = intent.getStringExtra("connectionType");


        Intent notificationIntent = new Intent(this, UserActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationIntent,0);

        String contextText;
        String provider;
        String user;
        if(connectionType.equals("local"))
        {
            contextText = "Now connected locally";
        }
        else
        {

            provider = intent.getStringExtra("provider");
            user = intent.getStringExtra("user");
            contextText = "Now connected remotely<br>Current session: "+user+"@"+provider;
        }

        SpannableString formattedBody = new SpannableString(Html.fromHtml(contextText));



        Notification notification= new NotificationCompat.Builder(this, App.id)
                .setContentTitle("Open Management System")
                .setContentText(formattedBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(formattedBody).setBigContentTitle("Open Management System"))
                .setSmallIcon(R.drawable.ic_close)
                .setContentIntent(null)
                .setOnlyAlertOnce(true)
                .setSound(null)
                .build();
        startForeground(1,notification);

        return START_NOT_STICKY;
        //return super.onStartCommand(intent, flags, startId);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
