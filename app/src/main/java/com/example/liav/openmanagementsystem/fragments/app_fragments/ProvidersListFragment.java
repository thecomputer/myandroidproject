package com.example.liav.openmanagementsystem.fragments.app_fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.activities.InputActivity;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;
import com.example.liav.openmanagementsystem.models.templates.InputActivityTypes;
import com.example.liav.openmanagementsystem.utils.recyclerview_adapters.TimetableEventsAdapter;

import java.util.ArrayList;

public class ProvidersListFragment extends Fragment {

    private boolean hasLoadedOnce = false;
    private int day;
    private Context context;


    private View inf;
    private int type;
    private int[] params;
    private ArrayList<TimetableEvent> timetableEvents;


    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;



    public void setType(int type, int params[])
    {
        this.type = type;
        this.params = params;
        this.day = params[0];
        //LoadTimeTableDayEvents();
    }
    public ProvidersListFragment()
    {
        super();

    }

    @Override
    public void onDetach() {
        this.context = null;
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;

        super.onAttach(context);

    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible) {
        super.setUserVisibleHint(true);

        if (this.isVisible()) {
            if (isFragmentVisible && hasLoadedOnce) {
                // start update code here so this code run only when user select this fragment
                this.updateExisting();
            }
        }
    }

    private void setListenerFloatingActionButton(View inf)
    {
        FloatingActionButton button = inf.findViewById(R.id.fab);
        final int day = this.day;
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent myIntent;

                myIntent = new Intent(getActivity(), InputActivity.class);
                myIntent.putExtra("type", InputActivityTypes.CLASS_B_CREATE._NAME);
                myIntent.putExtra("subtype", InputActivityTypes.CLASS_B_CREATE.TIMETABLE_EVENT);
                myIntent.putExtra("day", day);
                startActivity(myIntent);
            }
        });
    }



    private void PrintEvents(View inf)
    {
        recyclerView = (RecyclerView) inf.findViewById(R.id.eventsList);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(false);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this.context);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new TimetableEventsAdapter(this.timetableEvents);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        setListenerFloatingActionButton(inf);
    }

    public void updateExisting()
    {
        Log.i("attaching","updating!");
        if(this.hasLoadedOnce)
        {
            this.update();
        }
    }


    public void update()
    {
        View inf = this.inf;
        this.setType(this.type,this.params);
        PrintEvents(inf);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inf = inflater.inflate(R.layout.fragment_events, container, false);
        this.inf = inf;
        this.update();
        Log.i("Info","onCreateView Loaded for day "+this.day);
        this.hasLoadedOnce = true;
        return inf;
    }



}
