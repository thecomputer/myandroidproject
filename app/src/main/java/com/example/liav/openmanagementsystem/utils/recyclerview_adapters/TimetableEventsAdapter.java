package com.example.liav.openmanagementsystem.utils.recyclerview_adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;

import java.util.ArrayList;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.utils.layout.TabletimeEventsListBuilder;


public class TimetableEventsAdapter extends RecyclerView.Adapter {

    private ArrayList<TimetableEvent> timetableEventArrayList;

    public static class EventsViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Context context;
        public LinearLayout linearLayout;
        public TextView textTitle,textDesc,textNotes;

        public EventsViewHolder(LinearLayout v, Context context) {
            super(v);
            linearLayout = v;
            this.context = context;
            textTitle = v.findViewById(R.id.main_title);
            textDesc = v.findViewById(R.id.main_desc);
            textNotes = v.findViewById(R.id.main_notes);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TimetableEventsAdapter(ArrayList<TimetableEvent> events) {
        this.timetableEventArrayList = events;
    }

    public void setContents(ArrayList<TimetableEvent> events) {
        this.timetableEventArrayList = events;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TimetableEventsAdapter.EventsViewHolder onCreateViewHolder(ViewGroup parent,
                                                                      int viewType) {
        // create a new view
        //TextView v = (TextView) LayoutInflater.from(parent.getContext())
         //       .inflate(R.layout.my_text_view, parent, false);
        //LinearLayout l = TabletimeEventsListBuilder.buildTimetableEvents(new TimetableEvent(0,false,"","","",0,0,0),parent.getContext());
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_timetable_item,
                parent,
                false);


        EventsViewHolder vh = new EventsViewHolder(v,parent.getContext());
        vh.context = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        TabletimeEventsListBuilder.buildTimetableEvents(this.timetableEventArrayList.get(i),
                ((EventsViewHolder)viewHolder).linearLayout,
                ((EventsViewHolder)viewHolder).context);
    }
/*
    // Replace the contents of a view (invoked by the layout manager)
    public void onBindViewHolder(EventsViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


    }*/

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return timetableEventArrayList.size();
    }
}
