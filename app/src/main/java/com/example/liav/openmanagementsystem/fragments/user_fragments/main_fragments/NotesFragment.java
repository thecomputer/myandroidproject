package com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.activities.InputActivity;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.NotesListFragment;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.templates.InputActivityTypes;
import com.example.liav.openmanagementsystem.models.templates.NotesFragmentTypes;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;
import com.example.liav.openmanagementsystem.utils.UserDataManager;

import java.util.ArrayList;
import java.util.Calendar;

public class NotesFragment extends Fragment {


    private boolean attached;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    private UserDataManager dataManager;
    private Context context;

    private Fragment Fragment;
    private int selectTimeStamp;
    private int selectCategory;
    private int selectOrder;
    private boolean attachedOnce=false;

    private ArrayList<Note> notes;

    private View inf;

    public void setContext(Context context)
    {
        this.context = context;
    }

    private void releaseFragment(int i)
    {
        this.Fragment.onDestroy();
    }

    private int[] getSixMonthBefore(int year,int month)
    {
        int[] date = new int[2];
        date[0] = year;
        switch (month)
        {
            case(0): //jan
                date[0]--;
                date[1] = 6;
                break;
            case(1): //feb
                date[0]--;
                date[1] = 7;
                break;
            case(2): // mar
                date[0]--;
                date[1] = 8;
                break;
            case(3): // apr
                date[0]--;
                date[1] = 9;
                break;
            case(4): // may
                date[0]--;
                date[1] = 10;
                break;
            case(5): // jun
                date[0]--;
                date[1] = 11;
                break;
            case(6): // jul
                date[1] = 0;
                break;
            case(7): // aug
                date[1] = 1;
                break;
            case(8): // sep
                date[1] = 2;
                break;
            case(9): // oct
                date[1] = 3;
                break;
            case(10): // nov
                date[1] = 4;
                break;
            case(11): // dec
                date[1] = 5;
                break;
        }
        return date;
    }

    private int getMinimumTimeStamp(int selectTimeStamp)
    {
        Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
        switch (selectTimeStamp) {
            case NotesFragmentTypes.Timestamps.ALL:
                return 0;
            case NotesFragmentTypes.Timestamps.TODAY:
                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.HOUR_OF_DAY,0);
                c.set(Calendar.MINUTE,0);
                return (int)(c.getTimeInMillis()/1000);
            case NotesFragmentTypes.Timestamps.LAST_WEEK:
                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
                c.set(Calendar.HOUR_OF_DAY,0);
                c.set(Calendar.MINUTE,0);
                return (int)(c.getTimeInMillis()/1000);
            case NotesFragmentTypes.Timestamps.LAST_MONTH:
                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.DAY_OF_MONTH,1);
                c.set(Calendar.HOUR_OF_DAY,0);
                c.set(Calendar.MINUTE,0);
                return (int)(c.getTimeInMillis()/1000);
            case NotesFragmentTypes.Timestamps.LAST_6_MONTHS:
                c.setTimeInMillis(System.currentTimeMillis());

                int[] date = getSixMonthBefore(c.get(Calendar.YEAR),c.get(Calendar.MONTH));
                c.set(Calendar.MONTH,date[1]);
                c.set(Calendar.YEAR,date[0]);

                c.set(Calendar.DAY_OF_MONTH,1);
                c.set(Calendar.HOUR_OF_DAY,0);
                c.set(Calendar.MINUTE,0);
                return (int)(c.getTimeInMillis()/1000);
            case NotesFragmentTypes.Timestamps.LAST_YEAR:
                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.YEAR,Calendar.YEAR-1);
                c.set(Calendar.HOUR_OF_DAY,0);
                c.set(Calendar.MINUTE,0);
                return (int)(c.getTimeInMillis()/1000);
            case NotesFragmentTypes.Timestamps.LAST_3_YEARS:
                c.setTimeInMillis(System.currentTimeMillis());
                c.set(Calendar.YEAR,Calendar.YEAR-3);
                c.set(Calendar.HOUR_OF_DAY,0);
                c.set(Calendar.MINUTE,0);
                return (int)(c.getTimeInMillis()/1000);
            default:
                return 0;

        }

    }

    private Fragment GetFragment(ArrayList<Note> notes)
    {
        NotesListFragment fragment = new NotesListFragment();
        int[] params = {this.selectTimeStamp,this.selectCategory,this.selectOrder};
        ((NotesListFragment) fragment).setType(notes,this.context);
        return fragment;
    }

    private void changeFragment(ArrayList<Note> notes)
    {

        ArrayList<Note> odds = new ArrayList<>();
        ArrayList<Note> evens = new ArrayList<>();
        for(int i=0;i<notes.size();i=i+2)
        {
            odds.add(notes.get(i));
        }
        for(int i=1;i<notes.size();i=i+2)
        {
            evens.add(notes.get(i));
        }

        Fragment fragment = GetFragment(notes);
        this.Fragment = fragment;

        if(fragment instanceof NotesListFragment) {
            NotesListFragment notesListFragment = (NotesListFragment) fragment;
        }

        FragmentManager fragmentManager = getFragmentManager();//this.getFragmentManager();
        if(fragmentManager == null)
        {

            //fragmentManager = ((AppCompatActivity)this.context).getSupportFragmentManager();
            if(fragmentManager == null)
            {
                fragmentManager = getChildFragmentManager();
            }
        }
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.notesLayout1,GetFragment(odds));
        //fragmentTransaction.commit();

        //fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.notesLayout2,GetFragment(evens));
        fragmentTransaction.commit();

    }


    private static final int SELECTOR_UNDEFINED = -1;

    private void UpdateSelections(int TimeStampSelector,int CategorySelector,int OrderSelector)
    {
        if(TimeStampSelector != SELECTOR_UNDEFINED)
        {
            this.selectTimeStamp = TimeStampSelector;
        }
        if(CategorySelector != SELECTOR_UNDEFINED)
        {
            this.selectCategory = CategorySelector;
        }
        if(OrderSelector != SELECTOR_UNDEFINED)
        {
            this.selectOrder = OrderSelector;
        }
        this.updateFragment();
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;
        this.attached = true;
        super.onAttach(context);

    }

    private void createTimeStampSelector(View inf)
    {
        Spinner dropdown = inf.findViewById(R.id.timestamp_filter_spinner);
        String[] items = NotesFragmentTypes.getTimeStampSelectorValues();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                UpdateSelections(position,SELECTOR_UNDEFINED,SELECTOR_UNDEFINED);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }



    private void createCategorySelector(View inf)
    {
        Spinner dropdown = inf.findViewById(R.id.category_filter_spinner);
        String[] items = NotesFragmentTypes.getCategorySelectorValues();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                UpdateSelections(SELECTOR_UNDEFINED,position,SELECTOR_UNDEFINED);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void createOrderSelector(View inf)
    {
        Spinner dropdown = inf.findViewById(R.id.order_spinner);
        String[] items = NotesFragmentTypes.getOrderSelectorValues();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                UpdateSelections(SELECTOR_UNDEFINED,SELECTOR_UNDEFINED,position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    public void updateFragment()
    {
        if(this.attached == true && attachedOnce)
        {
            ArrayList<Note> notes = UserDataHolder.getInstance().getData().getNotes(
                    getMinimumTimeStamp(this.selectTimeStamp),
                    this.selectCategory,
                    this.selectOrder,this
            );
            changeFragment(notes);
        }
    }

    @Override
    public void onDetach() {
        this.attached = false;
        super.onDetach();
    }

    public void updateNotesFromThread(ArrayList<Note> notes)
    {
        this.notes = notes;
    }

    public void updateFragmentFromThread()
    {
        if(attachedOnce)
        {
            changeFragment(this.notes);
        }
    }


    private void setListenerFloatingActionButton(View inf)
    {
        FloatingActionButton button = inf.findViewById(R.id.fabNotes);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent myIntent;

                myIntent = new Intent(getActivity(), InputActivity.class);
                myIntent.putExtra("type", InputActivityTypes.CLASS_B_CREATE._NAME);
                myIntent.putExtra("subtype", InputActivityTypes.CLASS_B_CREATE.NOTES_CREATE);
                //myIntent.putExtra("current_timestamp", timeStamp);
                startActivity(myIntent);
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /**
         *Inflate tab_layout and setup Views.
         */
        View v = inflater.inflate(R.layout.fragment_notes, container, false);
        this.inf = v;
        UpdateSelections(NotesFragmentTypes.Timestamps.ALL,
                NotesFragmentTypes.SelectNotes._DATE_SELECTOR,
                NotesFragmentTypes.SelectNotes.DEFAULT_ORDER);

        createCategorySelector(v);
        createOrderSelector(v);
        createTimeStampSelector(v);

        setListenerFloatingActionButton(v);

        this.attachedOnce = true;

        return v;

    }

    public void setDataManager(UserDataManager dataManager)
    {
        this.dataManager = dataManager;
    }

}
