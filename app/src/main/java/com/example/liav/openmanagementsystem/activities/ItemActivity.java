package com.example.liav.openmanagementsystem.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.R;

public class ItemActivity extends Activity {


    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    //final private String[] type_of_items = {"event","note","other"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if(intent.getStringExtra("type").equals("event"))
        {
            setContentView(R.layout.activity_event_item);
            TextView title = findViewById(R.id.title);
            TextView desc = findViewById(R.id.desc);
            TextView timeDate = findViewById(R.id.timeDate);
            TextView notes = findViewById(R.id.notes);

            title.setText(intent.getStringExtra("title"));
            desc.setText(intent.getStringExtra("description"));
            timeDate.setText(intent.getStringExtra("time"));
            notes.setText(intent.getStringExtra("notes"));
        }
        else{
            if(intent.getStringExtra("type").equals("note"))
            {
                setContentView(R.layout.activity_note_item);
                TextView title = findViewById(R.id.title);
                TextView desc = findViewById(R.id.desc);
                title.setText(intent.getStringExtra("title"));
                desc.setText(intent.getStringExtra("description"));
            }
        }
    }
}
