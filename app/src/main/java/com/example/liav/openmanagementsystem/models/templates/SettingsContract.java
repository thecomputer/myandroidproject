//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.example.liav.openmanagementsystem.models.templates;

import android.content.ContentValues;
import android.provider.BaseColumns;

public final class SettingsContract {

    private SettingsContract() {}

    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "Settings";

        public static final String COLUMN1_NAME_TITLE = "setting_realname";
        public static final String COLUMN2_NAME_TITLE = "setting_name";
        public static final String COLUMN3_NAME_TITLE = "setting_value";
        public static final String COLUMN4_NAME_TITLE = "setting_category";
        public static final String COLUMN5_NAME_TITLE = "setting_type";
    }

    public static final String[] columns = {FeedEntry.COLUMN1_NAME_TITLE,FeedEntry.COLUMN2_NAME_TITLE,FeedEntry.COLUMN3_NAME_TITLE};

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN1_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN2_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN3_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN4_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN5_NAME_TITLE + " TEXT);";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    public static String InsertSettingQuery(String[] values_arr)
    {
        if(values_arr.length != 5)
            return null;
        return "INSERT INTO "+ FeedEntry.TABLE_NAME +" (" +
                FeedEntry.COLUMN1_NAME_TITLE+","+
                FeedEntry.COLUMN2_NAME_TITLE+","+
                FeedEntry.COLUMN3_NAME_TITLE+","+
                FeedEntry.COLUMN4_NAME_TITLE+","+
                FeedEntry.COLUMN5_NAME_TITLE+") VALUES ("+
                "'"+values_arr[0]+"',"+
                "'"+values_arr[1]+"',"+
                "'"+values_arr[2]+"',"+
                "'"+values_arr[3]+"',"+
                "'"+values_arr[4]+"');";

    }

    public static int getAmountOfSettingsInCatergory(String value)
    {
        int i = 0;

        for (int j=0;j<DefaultSettings.length; j++)
        {
            if(DefaultSettings[j][2] == value)
                i++;
        }
        return i;
    }

    // 3 categories - general, appearance, notifications,
    // Also - General Info is needed, like in youtube app

    public static final String[] SettingsTypes = {"text","boolean","numerical"};

    public static final String[] DefaultSettingsCategories = {"general","appearance","notifications","notifications-verbose","localuser"};

    public static final String[][] DefaultSettings = new String[][]{

            // general
            {"","allow_insecure_connections","false","",""},
            //{"Enable Admin Mode","enable_adminmode","false",DefaultSettingsCategories[0],SettingsTypes[1]},
            //{"Enable Local User","enable_localuser","false",DefaultSettingsCategories[0],SettingsTypes[1]},
            {"","warn_before_exiting_local_session","false","",""},
            {"","timezone","0","",""}
            //{"Enable Remember Login Token","enable_remember_login","true",DefaultSettingsCategories[0],SettingsTypes[1]},

            // theme

            //{"Enable Quick Links On Main Screen","enable_quick_link_main_screen","true",DefaultSettingsCategories[1],SettingsTypes[1]},
            //{"Enable Statistics on Main Screen","enable_stats_main_screen","true",DefaultSettingsCategories[1],SettingsTypes[1]},
            //{"Main theme","theme","default",DefaultSettingsCategories[1],SettingsTypes[0]},

            // notifications

            //{"Enable verbose mode","verbose_mode","false",DefaultSettingsCategories[2],SettingsTypes[1]},
            //{"Enable for Remote Admin User Session","verbose_mode_remote_admin","false",DefaultSettingsCategories[2],SettingsTypes[1]},
            //{"Enable for Local User Session","verbose_mode_localuser","false",DefaultSettingsCategories[2],SettingsTypes[1]},
            //{"Enable for Remote User Session","verbose_mode_remote_user","false",DefaultSettingsCategories[2],SettingsTypes[1]},

            //{"Enable for Local User Sessions","enable_notifications_local_user","false",DefaultSettingsCategories[3],SettingsTypes[1]},
            //{"Enable for Remote User Sessions","enable_notifications_remote_user","false",DefaultSettingsCategories[3],SettingsTypes[1]},
            //{"Enable for Admin Remote User Sessions","enable_admin_remote_user_notifications","false",DefaultSettingsCategories[3],SettingsTypes[1]}

    };
    //
    public static ContentValues PrepareWriteToTable(String[] values_arr)
    {
        if(values_arr.length != 5)
            return null;
// Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedEntry.COLUMN1_NAME_TITLE, values_arr[0]);
        values.put(FeedEntry.COLUMN2_NAME_TITLE, values_arr[1]);
        values.put(FeedEntry.COLUMN3_NAME_TITLE, values_arr[2]);
        values.put(FeedEntry.COLUMN4_NAME_TITLE, values_arr[3]);
        values.put(FeedEntry.COLUMN5_NAME_TITLE, values_arr[4]);
// Insert the new row, returning the primary key value of the new row
        return values;
    }

}
