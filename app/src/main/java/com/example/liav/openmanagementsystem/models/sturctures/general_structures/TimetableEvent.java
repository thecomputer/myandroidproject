package com.example.liav.openmanagementsystem.models.sturctures.general_structures;

public class TimetableEvent {

    private boolean notification_enabled;
    private String notes;
    private String title;
    private String description;
    private int day;
    private int hour;
    private int minute;

    private int id;

    public TimetableEvent(int id, boolean notification_enabled, String title, String description, String notes, int day, int hour, int minute) {
        this.notification_enabled = notification_enabled;
        this.notes = notes;
        this.title = title;
        this.description = description;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.id = id;
    }

    public boolean isNotification_enabled() {
        return notification_enabled;
    }

    public String getNotes() {
        return notes;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getID() {
        return id;
    }

    @Override
    public String toString() {
        return "build"+this.id;
    }
}
