package com.example.liav.openmanagementsystem.utils.timezone.llttz;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TimeZone;

import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;
import com.example.liav.openmanagementsystem.utils.timezone.llttz.stores.Location;
import com.example.liav.openmanagementsystem.utils.timezone.llttz.stores.TimeZoneStore;

/**
 * @author artemgapchenko
 * Created on 18.04.14.
 */
public class Converter implements IConverter {
    private TimeZoneStore tzStore;
    private static Converter instance = null;

    private Converter(Class clazz) {
        if (!TimeZoneStore.class.isAssignableFrom(clazz)) {
            throw new IllegalArgumentException("Illegal store provided: " + clazz.getName());
        }
        try {
            tzStore = (TimeZoneStore) clazz.newInstance();
            loadData();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static Converter getInstance(final Class clazz) {
        if (instance == null || !instance.getStoreClass().equals(clazz)) instance = new Converter(clazz);
        return instance;
    }

    public Class getStoreClass() {
        return tzStore.getClass();
    }

    @Override
    public TimeZone getTimeZone(final double lat, final double lon) {
        return tzStore.nearestTimeZone(new Location(new double[]{lat, lon}));
    }

    private void loadData() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(AppDataHolder.getInstance().getData().getContext().getAssets().open("timezones.csv")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            String line;
            String[] location;

            while ((line = br.readLine()) != null) {
                location = line.split(";");
                tzStore.insert(new Location(Double.valueOf(location[1]), Double.valueOf(location[2]), location[0]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}