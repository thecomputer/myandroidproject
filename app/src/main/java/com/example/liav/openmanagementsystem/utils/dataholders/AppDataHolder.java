package com.example.liav.openmanagementsystem.utils.dataholders;

import com.example.liav.openmanagementsystem.utils.AppDataManager;

public class AppDataHolder {
    private AppDataManager data;
    public AppDataManager getData() {return data;}
    public void setData(AppDataManager data) {this.data = data;}

    private static final AppDataHolder holder = new AppDataHolder();
    public static AppDataHolder getInstance() {return holder;}
}