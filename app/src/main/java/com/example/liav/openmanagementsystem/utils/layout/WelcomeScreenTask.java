package com.example.liav.openmanagementsystem.utils.layout;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.widget.ProgressBar;

import com.example.liav.openmanagementsystem.activities.MainActivity;

public class WelcomeScreenTask extends AsyncTask<Void, Integer, Integer>
{
    private Context context;
    private ProgressBar progressBar;

    public WelcomeScreenTask(Context context, ProgressBar progressBar){
    this.context = context;
    this.progressBar = progressBar;
    }

    protected void onPreExecute()
    {
        progressBar.setMax(100);
    }

    protected void publishProgress(int progress)
    {
        progressBar.setProgress(progress);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Integer doInBackground(Void... voids)
    {
        int progress_status=0;
        while (progress_status < 100){
            progress_status +=1;
            publishProgress(progress_status);
            SystemClock.sleep(5);
        }
        return null;
    }
    protected void onPostExecute(Integer intd)
    {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

}