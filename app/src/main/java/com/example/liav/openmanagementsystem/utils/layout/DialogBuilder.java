package com.example.liav.openmanagementsystem.utils.layout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.LinearLayout;

import com.example.liav.openmanagementsystem.activities.InputActivity;
import com.example.liav.openmanagementsystem.activities.ItemActivity;
import com.example.liav.openmanagementsystem.models.templates.InputActivityTypes;

import java.text.DecimalFormat;

public class DialogBuilder {

    private DialogBuilder() {}


    public static void setDialogAccountList(String[] options, Context classContext, int layoutID)
    {
        final Context context = classContext;
        final int id = layoutID;

        DialogInterface.OnClickListener actionListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent myIntent;
                switch (which) {
                    case 0: // Add new events
                        Log.i("op","edit " +id);
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_A_EDIT._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_A_EDIT.USER_EDIT);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    case 1: // Delete
                        Log.i("op","delete " +id);
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_A_DELETE._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_A_DELETE.USER_DELETE);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    default:
                        break;
                }
            }
        };
        AlertDialog actions;
        AlertDialog.Builder builder = new AlertDialog.Builder(classContext);

        builder.setItems(options, actionListener);
        builder.setNegativeButton("Cancel", null);
        actions = builder.create();
        actions.setTitle("Account " + (id+1));

        actions.show();
    }

    public static void setDialogProviderList(String[] options, Context classContext, int layoutID)
    {
        final Context context = classContext;
        final int id = layoutID;

        DialogInterface.OnClickListener actionListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent myIntent;
                switch (which) {
                    case 0: // Add new events
                        Log.i("op","edit " +id);
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_A_EDIT._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_A_EDIT.PROVIDER_EDIT);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    case 1: // Delete
                        Log.i("op","delete " +id);
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_A_DELETE._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_A_DELETE.PROVIDER_DELETE);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    default:
                        break;
                }
            }
        };
        AlertDialog actions;
        AlertDialog.Builder builder = new AlertDialog.Builder(classContext);

        builder.setItems(options, actionListener);
        builder.setNegativeButton("Cancel", null);
        actions = builder.create();
        actions.setTitle("Provider " + (id+1));

        actions.show();
    }

    public static void setDialogEventList(String[] options, Context classContext, int layoutID,int hour,int minute)
    {
        final Context context = classContext;
        final int id = layoutID;

        DecimalFormat formatter = new DecimalFormat("00");
        String Minute = formatter.format(minute);

        DialogInterface.OnClickListener actionListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent myIntent;
                switch (which) {
                    case 0: // Add new events
                        Log.i("op","edit " +id);
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_B_EDIT._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_B_EDIT.TIMETABLE_EVENT);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    case 1: // Delete
                        Log.i("op","delete " +id);
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_B_DELETE._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_B_DELETE.TIMETABLE_EVENT);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    default:
                        break;
                }
            }
        };
        AlertDialog actions;
        AlertDialog.Builder builder = new AlertDialog.Builder(classContext);

        builder.setItems(options, actionListener);
        builder.setNegativeButton("Cancel", null);
        actions = builder.create();
        actions.setTitle("Event at " + hour + ":" + Minute);

        actions.show();
    }

    public static void setDialogCalendarEventList(String[] options, Context classContext, int layoutID,int hour, int minute,int year,int month, int day)
    {
        final Context context = classContext;
        final int id = layoutID;

        DecimalFormat formatter = new DecimalFormat("00");
        String Minute = formatter.format(minute);

        DialogInterface.OnClickListener actionListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent myIntent;
                switch (which) {
                    case 0: // Add new events
                        Log.i("op","edit " +id);
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_B_EDIT._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_B_EDIT.CALENDAR_EVENT);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    case 1: // Delete
                        Log.i("op","delete " +id);
                        //Intent myIntent;
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_B_DELETE._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_B_DELETE.CALENDAR_EVENT);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    default:
                        break;
                }
            }
        };
        AlertDialog actions;
        AlertDialog.Builder builder = new AlertDialog.Builder(classContext);

        builder.setItems(options, actionListener);
        builder.setNegativeButton("Cancel", null);
        actions = builder.create();
        actions.setTitle("Event at " + day+"."+(month+1)+"."+year+" - " + hour + ":" + Minute);

        actions.show();
    }

    public static void setDialogNoteList(String[] options, Context classContext, int layoutID,String title)
    {
        final Context context = classContext;
        final int id = layoutID;

        DialogInterface.OnClickListener actionListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent myIntent;
                switch (which) {
                    case 0: // Add new events
                        Log.i("op","edit " +id);
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_B_EDIT._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_B_EDIT.NOTES);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    case 1: // Delete
                        Log.i("op","delete " +id);
                        //Intent myIntent;
                        myIntent = new Intent(context, InputActivity.class);
                        myIntent.putExtra("type", InputActivityTypes.CLASS_B_DELETE._NAME);
                        myIntent.putExtra("subtype",InputActivityTypes.CLASS_B_DELETE.NOTES);
                        myIntent.putExtra("id",id);
                        context.startActivity(myIntent);
                        break;
                    default:
                        break;
                }
            }
        };
        AlertDialog actions;
        AlertDialog.Builder builder = new AlertDialog.Builder(classContext);

        builder.setItems(options, actionListener);
        builder.setNegativeButton("Cancel", null);
        actions = builder.create();
        actions.setTitle(title);

        actions.show();
    }


}
