package com.example.liav.openmanagementsystem.fragments.user_fragments.sub_fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;

import java.text.DecimalFormat;
import java.util.Calendar;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    private int hour;
    private int minute;
    private TextView textView;


    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public void setTextView(TextView textView)
    {
        this.textView = textView;
    }

    public void setTime(int hour,int minute)
    {
        this.hour = hour;
        this.minute = minute;
    }

    public void setDefaultTime()
    {
        Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
        int minute = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        this.hour = hour;
        this.minute = minute;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Show picker with actual date presetted
        //Calendar c = Calendar.getInstance();
        //int minute = c.get(Calendar.MINUTE);
        //int hour = c.get(Calendar.HOUR_OF_DAY);

        // Create a new instance of DatePickerDialog and return it
        return new TimePickerDialog(getActivity(),this,this.hour,this.minute,true);
    }

    public void onTimeSet(TimePicker view, int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
        this.setTextTime();
    }
    private void setTextTime()
    {
        DecimalFormat formatter = new DecimalFormat("00");
        String Minute = formatter.format(this.minute);
        this.textView.setText(""+this.hour+":"+Minute);
    }
}