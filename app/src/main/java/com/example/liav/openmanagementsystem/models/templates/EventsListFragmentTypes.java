package com.example.liav.openmanagementsystem.models.templates;

public final class EventsListFragmentTypes {
    private EventsListFragmentTypes() {}

    public final static class TimeTableEvents {
        public static final int _CLASS = 0x0;
        public static final int DESC_ORDER = 0x1;
        public static final int ASC_ORDER = 0x0;
        public static final int[] days = {0,1,2,3,4,5,6};
        public static final int DEFAULT_ORDER = ASC_ORDER ;
    }

    public final static class CalendarEventsToday {
        public static final int _CLASS = 0x1;
        public static final int DESC_ORDER = 0x1;
        public static final int ASC_ORDER = 0x0;
        public static final int DEFAULT_ORDER = ASC_ORDER ;
    }

    public final static class CalendarEventsIncoming {
        public static final int _CLASS = 0x2;
        public static final int DESC_ORDER = 0x1;
        public static final int ASC_ORDER = 0x0;
        public static final int DEFAULT_ORDER = DESC_ORDER ;
    }

    public final static class CalendarEventsAll {
        public static final int _CLASS = 0x3;
        public static final int DESC_ORDER = 0x1;
        public static final int ASC_ORDER = 0x0;
        public static final int DEFAULT_ORDER = DESC_ORDER ;
    }

    public final static class CalendarEventsDay {
        public static final int _CLASS = 0x4;
        public static final int DESC_ORDER = 0x1;
        public static final int ASC_ORDER = 0x0;
        public static final int DEFAULT_ORDER = DESC_ORDER ;
    }
}
