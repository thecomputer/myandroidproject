package com.example.liav.openmanagementsystem.utils.recyclerview_adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.utils.layout.CalendarEventsListBuilder;

import java.util.ArrayList;


public class CalendarEventsAdapter extends RecyclerView.Adapter {

    private ArrayList<CalendarEvent> calendarEventArrayList;

    public static class EventsViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Context context;
        public LinearLayout linearLayout;
        public TextView textTitle,textDesc,textDate;

        public EventsViewHolder(LinearLayout v, Context context) {
            super(v);
            linearLayout = v;
            this.context = context;
            textDate = v.findViewById(R.id.main_date);
            textTitle = v.findViewById(R.id.main_title);
            textDesc = v.findViewById(R.id.main_desc);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CalendarEventsAdapter(ArrayList<CalendarEvent> events) {
        this.calendarEventArrayList = events;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CalendarEventsAdapter.EventsViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        // create a new view
        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calendar_item,
                parent,
                false);
        EventsViewHolder vh = new EventsViewHolder(v,parent.getContext());
        vh.context = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        CalendarEventsListBuilder.buildCalendarEvents(this.calendarEventArrayList.get(i),
                ((EventsViewHolder)viewHolder).linearLayout,
                ((EventsViewHolder)viewHolder).context);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.calendarEventArrayList.size();
    }
}
