package com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.activities.InputActivity;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.CalendarEventsListFragment;
import com.example.liav.openmanagementsystem.models.templates.EventsListFragmentTypes;
import com.example.liav.openmanagementsystem.models.templates.InputActivityTypes;

public class CalendarFragment extends Fragment {
    private Context context;
    private AlertDialog actions;
    @Override
    public void onAttach(Context context) {
        this.context = context;
        super.onAttach(context);
        //LoadDayEvents(this.day);



    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        /*AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
        builder.setTitle("Choose an Option");
        String[] options = { "Add new event", "See events" };
        builder.setItems(options, actionListener);
        builder.setNegativeButton("Cancel", null);
        actions = builder.create();*/

        View inf = inflater.inflate(R.layout.fragment_calendar, container, false);

        CalendarView calendarView = inf.findViewById(R.id.calendarView2);
        final FragmentManager fragmentManager = this.getFragmentManager();


        CalendarEventsListFragment fragment = new CalendarEventsListFragment();
        int[] params = {0,EventsListFragmentTypes.CalendarEventsToday.DEFAULT_ORDER};
        fragment.setType(EventsListFragmentTypes.CalendarEventsToday._CLASS,params);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.calendarLayout,fragment);
        fragmentTransaction.commit();


        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                int daySelected = dayOfMonth;
                int yearSelected = year;
                int monthSelected = month;

                CalendarEventsListFragment fragment = new CalendarEventsListFragment();
                int[] params = {daySelected,monthSelected,yearSelected,EventsListFragmentTypes.CalendarEventsDay.DEFAULT_ORDER};
                fragment.setType(EventsListFragmentTypes.CalendarEventsDay._CLASS,params);


                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.calendarLayout,fragment);
                fragmentTransaction.commit();
                Log.i("date",""+daySelected+" "+monthSelected+" "+yearSelected);

            }
        });
        return inf;
    }

}
