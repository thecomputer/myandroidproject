package com.example.liav.openmanagementsystem.models.sturctures.user_structures;


public class Account {


    public static final String LOCAL_PROVIDER = "local://localphone/";
    private int id;
    private String username;
    private String password;
    private String email;
    private String provider_uri;
    private String hash_strength_provider;

    private String token;
    private int permission_role;

    public Account(String username,String password,String email,String provider_uri,String hash_strength_provider)
    {
        this.username = username;
        this.email = email;
        this.provider_uri = provider_uri;
        this.hash_strength_provider = hash_strength_provider;
        this.password = password;
    }
    public Account(String username,String password,String email,String provider_uri,String hash_strength_provider,String token,int permission_role)
    {
        this.username = username;
        this.email = email;
        this.provider_uri = provider_uri;
        this.hash_strength_provider = hash_strength_provider;
        this.password = password;

        this.token = token;
        this.permission_role = permission_role;
    }

    public void setID(int id)
    {
        this.id = id;
    }
    public int getID()
    {
        return this.id;
    }

    public String getEmail() {
        return email;
    }

    public String getHash_strength_provider() {
        return hash_strength_provider;
    }

    public String getPassword() {
        return password;
    }

    public String getProvider_uri() {
        return provider_uri;
    }

    public String getToken() { return this.token; }

    public String getUsername() {
        return username;
    }

    public int getPermission_role() { return this.permission_role; }

    public String[] getValues() {

        String[] strs = {this.username,this.password,this.email,this.provider_uri,this.hash_strength_provider};
        return  strs;
    }
}
