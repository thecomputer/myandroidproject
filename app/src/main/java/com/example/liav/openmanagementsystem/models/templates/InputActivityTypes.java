package com.example.liav.openmanagementsystem.models.templates;

public final class InputActivityTypes {
    private InputActivityTypes() {}

    public final static class CLASS_A_CREATE {
        public static final String _NAME = "class-a-create";
        public static final String USER_REGISTER = "new-user";
        public static final String PROVIDER_REGISTER = "new-provider";
    }

    public final static class CLASS_A_EDIT {
        public static final String _NAME = "class-a-edit";
        public static final String USER_EDIT = "edit-user";
        public static final String PROVIDER_EDIT = "edit-provider";
    }

    public final static class CLASS_A_DELETE {
        public static final String _NAME = "class-a-delete";
        public static final String USER_DELETE = "delete-user";
        public static final String PROVIDER_DELETE = "delete-provider";
    }

    public final static class CLASS_B_CREATE {
        public static final String _NAME = "class-b-create";
        public static final String CALENDAR_EVENT = "calendar-event-new";
        public static final String TIMETABLE_EVENT = "timetable-event-new";
        public static final String NOTES_CREATE = "notes-new";
    }

    public final static class CLASS_B_EDIT {
        public static final String _NAME = "class-b-edit";
        public static final String CALENDAR_EVENT = "calendar-event-edit";
        public static final String TIMETABLE_EVENT = "timetable-event-editing";
        public static final String NOTES = "notes-edit";
    }

    public final static class CLASS_B_DELETE {
        public static final String _NAME = "class-b-delete";
        public static final String CALENDAR_EVENT = "calendar-event-delete";
        public static final String TIMETABLE_EVENT = "timetable-event-delete";
        public static final String NOTES = "notes-delete";
    }

    public final static class CLASS_C_CREATE {
        public static final String _NAME = "class-c-create";
        public static final String USER_REGISTER = "new-user";
    }

    public final static class CLASS_C_EDIT {
        public static final String _NAME = "class-c-edit";
        public static final String USER_EDIT = "edit-user";
    }

    public final static class CLASS_C_DELETE {
        public static final String _NAME = "class-c-delete";
        public static final String USER_DELETE = "delete-user";
    }
}
