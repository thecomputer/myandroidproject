package com.example.liav.openmanagementsystem.fragments.user_fragments.sub_fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    private int year;
    private int month;
    private int day;
    private TextView textView;


    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public void setTextView(TextView textView)
    {
        this.textView = textView;
    }

    public void setDefaultDate()
    {
        Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
        this.year = c.get(Calendar.YEAR);
        this.month = c.get(Calendar.MONTH);
        this.day = c.get(Calendar.DAY_OF_MONTH);
    }
    public void setDate(int year,int month,int day)
    {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //Show picker with actual date presetted
        //final Calendar c = Calendar.getInstance();
        ///int year = c.get(Calendar.YEAR);
        //int month = c.get(Calendar.MONTH);
        //int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, this.year, this.month, this.day);
    }

    @Override
    public void onDateSet(DatePicker view,int year,int month, int day) {
        this.day = day;
        this.month = month;
        this.year = year;

        final Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR,year);
        c.set(Calendar.MONTH,month);
        c.set(Calendar.DAY_OF_MONTH,day);
        this.textView.setText(this.day+"/"+(this.month+1)+"/"+this.year);
    }
}