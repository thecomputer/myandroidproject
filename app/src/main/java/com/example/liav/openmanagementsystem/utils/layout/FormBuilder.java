package com.example.liav.openmanagementsystem.utils.layout;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.fragments.user_fragments.sub_fragments.DatePickerFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.sub_fragments.TimePickerFragment;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Provider;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.CalendarContract;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;

public class FormBuilder {
    private FormBuilder() {}

    public static void buildLoginForm()
    {

    }
    public static void buildRegisterUserForm()
    {

    }

    //

    private static void AddView(LinearLayout inputLayout,View formLayout)
    {
        inputLayout.addView(formLayout);
    }

    public static void buildCalendarDateForm(LinearLayout inputLayout, final FragmentManager fragmentManager, View formLayout, final CalendarEvent values)
    {
        AddView(inputLayout,formLayout);



        final TextView datePicker = (TextView) formLayout.findViewById(R.id.datePicker);
        datePicker.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                //HashMap<String,String> values = FormBuilder.getTimetableDateFormResults(getWindow().getDecorView().getRootView());
                //values.put("day",""+day);
                //UserDataHolder.getInstance().getData().createTimetableEvent(values);
                DialogFragment newFragment = new DatePickerFragment();
                ((DatePickerFragment)newFragment).setTextView(datePicker);
                if(values != null)
                {
                    Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
                    c.setTimeInMillis(values.getTimestamp()*1000);
                    ((DatePickerFragment)newFragment).setDate(c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH));
                }
                else
                {

                    ((DatePickerFragment)newFragment).setDefaultDate();

                }


                newFragment.show(fragmentManager, "Select date");
            }
        });

        final TextView timePicker = (TextView) formLayout.findViewById(R.id.timePicker);
        timePicker.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new TimePickerFragment();
                ((TimePickerFragment)newFragment).setTextView(timePicker);
                if(values != null)
                {
                    Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
                    c.setTimeInMillis(values.getTimestamp()*1000);
                    ((TimePickerFragment)newFragment).setTime(c.get(Calendar.HOUR_OF_DAY),c.get(Calendar.MINUTE));
                }
                else
                {
                    ((TimePickerFragment)newFragment).setDefaultTime();
                }


                newFragment.show(fragmentManager, "Select time");
            }
        });
        if(values != null)
        {
            Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
            Date date = new Date(0);
            date.setTime(values.getTimestamp()*1000);
            c.setTime(date);
            setPickers(datePicker,timePicker,c);


            EditText inputTitle = (EditText) formLayout.findViewById(R.id.inputTitle);
            EditText inputDesc = (EditText) formLayout.findViewById(R.id.inputDesc);
            EditText inputNotes = (EditText) formLayout.findViewById(R.id.inputNotesEvent);

            inputDesc.setText(values.getDescription().toString());
            inputTitle.setText(values.getTitle().toString());
            inputNotes.setText(values.getNotes().toString());

        }
        else
        {
            Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
            setPickers(datePicker,timePicker,c);
        }
    }

    private static void setPickers(TextView datePicker, TextView timePicker,Calendar c)
    {
        int minute = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int year = c.get(Calendar.YEAR);
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH)+1;

        DecimalFormat formatter = new DecimalFormat("00");
        String Minute = formatter.format(minute);

        timePicker.setText(""+hour+":"+Minute);
        datePicker.setText(""+day+"/"+month+"/"+year);
    }

    public static void buildTimetableDateForm(LinearLayout inputLayout, final FragmentManager fragmentManager, View formLayout, final TimetableEvent values)
    {
        AddView(inputLayout,formLayout);
        final TextView timePicker = (TextView) formLayout.findViewById(R.id.timePicker);
        timePicker.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                //HashMap<String,String> values = FormBuilder.getTimetableDateFormResults(getWindow().getDecorView().getRootView());
                //values.put("day",""+day);
                //UserDataHolder.getInstance().getData().createTimetableEvent(values);
                DialogFragment newFragment = new TimePickerFragment();
                ((TimePickerFragment)newFragment).setTextView(timePicker);
                if(values != null)
                {
                    ((TimePickerFragment)newFragment).setTime(values.getHour(),values.getMinute());
                }
                else
                {
                    ((TimePickerFragment)newFragment).setDefaultTime();

                }
                newFragment.show(fragmentManager, "Select time");
            }
        });
        if(values != null)
        {
            DecimalFormat formatter = new DecimalFormat("00");
            String Minute = formatter.format(values.getMinute());
            timePicker.setText(""+values.getHour()+":"+Minute);
            EditText inputTitle = (EditText) formLayout.findViewById(R.id.inputTitle);
            EditText inputDesc = (EditText) formLayout.findViewById(R.id.inputDesc);
            EditText inputNotes = (EditText) formLayout.findViewById(R.id.inputNotesEvent);

            inputDesc.setText(values.getDescription().toString());
            inputTitle.setText(values.getTitle().toString());
            inputNotes.setText(values.getNotes().toString());

        }
        else
        {
            Calendar c = Calendar.getInstance();
            int minute = c.get(Calendar.MINUTE);
            int hour = c.get(Calendar.HOUR_OF_DAY);
            DecimalFormat formatter = new DecimalFormat("00");
            String Minute = formatter.format(minute);
            timePicker.setText(""+hour+":"+Minute);
        }
    }

    public static void buildNotesForm(LinearLayout inputLayout, View formLayout, Note values)
    {
        AddView(inputLayout,formLayout);
        if(values != null)
        {
            EditText inputTitle = (EditText) formLayout.findViewById(R.id.NoteTitle);
            EditText inputDesc = (EditText) formLayout.findViewById(R.id.NoteDesc);

            inputDesc.setText(values.getDesc());
            inputTitle.setText(values.getTitle());
        }
    }

    public static void buildUserForm(LinearLayout inputLayout, View formLayout, Account values)
    {
        AddView(inputLayout,formLayout);
        Spinner spinnerProvider = formLayout.findViewById(R.id.providerSelect);
        String[] arraySpinner = AppDataHolder.getInstance().getData().UpdateProvidersURIArray();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AppDataHolder.getInstance().getData().getContext(),
                android.R.layout.simple_spinner_dropdown_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProvider.setAdapter(adapter);

        if(values != null)
        {
            EditText username = formLayout.findViewById(R.id.editUsername);
            //EditText password = formLayout.findViewById(R.id.editPassword);

            username.setText(values.getUsername());
            //password.setText(values.getPassword());

        }
    }

    public static void buildProviderForm(LinearLayout inputLayout, View formLayout, Provider values)
    {
        AddView(inputLayout,formLayout);
        Spinner spinnerProtocol = formLayout.findViewById(R.id.protocolSelect);
        String[] arraySpinner = new String[] {
                "http://","https://"
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AppDataHolder.getInstance().getData().getContext(),
                android.R.layout.simple_spinner_dropdown_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProtocol.setAdapter(adapter);
        if(values != null)
        {
            try {

                EditText name = formLayout.findViewById(R.id.nameProvider);
                name.setText(values.getProvider_name());

                EditText inputURI = (EditText) formLayout.findViewById(R.id.uriProvider);
                Spinner spinner = (Spinner) formLayout.findViewById(R.id.protocolSelect);
                URL url = new URL(values.getProvider_uri());

                String protocol = url.getProtocol();
                String server = url.getAuthority();

                inputURI.setText(values.getProvider_uri());
                inputURI.setText(server);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


        }
    }

    //

    public static Provider getProviderFormResults(View inf)
    {
        EditText name = (EditText)inf.findViewById(R.id.nameProvider);
        Spinner spinner = inf.findViewById(R.id.protocolSelect);
        String protocol = spinner.getSelectedItem().toString();
        EditText uri = (EditText)inf.findViewById(R.id.uriProvider);
        return new Provider(0,name.getText().toString(),protocol+uri.getText().toString(),"");
    }

    public static Account getUserFormResults(View inf)
    {
        EditText name = (EditText)inf.findViewById(R.id.editUsername);
        EditText pass = (EditText)inf.findViewById(R.id.editPassword);

        Spinner spinner = inf.findViewById(R.id.providerSelect);
        String uri = spinner.getSelectedItem().toString();
        return new Account(name.getText().toString(),pass.getText().toString(),"",uri,"");
    }

    public static HashMap<String,String> getCalendarDateFormResults(View inf)
    {
        HashMap<String,String> hashMap = getGeneralEventFormResults(inf);
        Calendar c = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
        TextView inputDate = (TextView) inf.findViewById(R.id.datePicker);
        String[] date = inputDate.getText().toString().split("/");

        int year = Integer.parseInt(date[2]);
        int month = Integer.parseInt(date[1]);
        int day = Integer.parseInt(date[0]);
        c.set(Calendar.MONTH,month-1);
        c.set(Calendar.DAY_OF_MONTH,day);
        c.set(Calendar.YEAR,year);
        c.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hashMap.get("hour")));
        c.set(Calendar.MINUTE,Integer.parseInt(hashMap.get("minute")));

        long timestamp = (c.getTimeInMillis()/1000);
        hashMap.remove("hour");
        hashMap.remove("minute");
        hashMap.put(CalendarContract.FeedEntry.COLUMN1_NAME_TITLE,""+timestamp);

        return hashMap;
    }

    public static HashMap<String,String> getTimetableDateFormResults(View inf)
    {
        return getGeneralEventFormResults(inf);

    }

    private static HashMap<String,String> getGeneralEventFormResults(View inf)
    {
        HashMap<String,String> hashMap = new HashMap<String, String>();

        EditText inputTitle = (EditText) inf.findViewById(R.id.inputTitle);
        EditText inputDesc = (EditText) inf.findViewById(R.id.inputDesc);
        EditText inputNotes = (EditText) inf.findViewById(R.id.inputNotesEvent);
        TextView inputTime = (TextView) inf.findViewById(R.id.timePicker);

        hashMap.put("title",inputTitle.getText().toString());
        hashMap.put("desc",inputDesc.getText().toString());
        hashMap.put("notes",inputNotes.getText().toString());

        String[] time = inputTime.getText().toString().split(":");
        hashMap.put("hour",time[0]);
        hashMap.put("minute",time[1]);
        return hashMap;
    }

    public static HashMap<String,String> getNotesFormResults(View inf)
    {
        HashMap<String,String> hashMap = new HashMap<String, String>();

        EditText inputTitle = (EditText) inf.findViewById(R.id.NoteTitle);
        EditText inputDesc = (EditText) inf.findViewById(R.id.NoteDesc);

        hashMap.put("note_title",inputTitle.getText().toString());
        hashMap.put("note_description",inputDesc.getText().toString());

        return hashMap;

    }
}