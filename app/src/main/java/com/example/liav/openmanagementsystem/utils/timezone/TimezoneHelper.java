package com.example.liav.openmanagementsystem.utils.timezone;

import android.util.Log;

import com.example.liav.openmanagementsystem.utils.timezone.llttz.Converter;
import com.example.liav.openmanagementsystem.utils.timezone.llttz.stores.TimeZoneListStore;

import java.util.Calendar;
import java.util.TimeZone;

public class TimezoneHelper {

    public static final int SECONDS_IN_HOUR = 3600;
    public static final int MILISECONDS_TO_SECOND = 1000;
    public static final int DEFAULT_ANDROID_TIMEZONE = -777;
    public static final int REQUEST_GPS_USAGE = -999;


    private int timezone;

    public TimezoneHelper(int timezone)
    {
        this.timezone = timezone;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    public long getShiftedTimestamp(long utc_timestamp)
    {
        int timezoneOffset = SECONDS_IN_HOUR*this.timezone;
        return utc_timestamp+timezoneOffset;
    }
    public long getUTC_Timestamp(long shifted_timestamp)
    {
        int timezoneOffset = SECONDS_IN_HOUR*this.timezone;
        return shifted_timestamp-timezoneOffset;
    }

    public TimeZone getGPS_Timezone()
    {
        Converter iconv = Converter.getInstance(TimeZoneListStore.class);

        TimeZone tz = iconv.getTimeZone(GPSHelper.getLatitude(), GPSHelper.getLongitude());
        //TimeZone tz = iconv.getTimeZone(40.741895,-73.989308);
        Log.i("Timezone","Latitude: "+GPSHelper.getLatitude());
        Log.i("Timezone","Longtitude: "+GPSHelper.getLongitude());

        Log.i("Timezone",tz.toString());

        return tz;
    }

    public Calendar getCalendar()
    {
        int timezoneOffset = this.timezone;
        Calendar cal = Calendar.getInstance();

        if(timezoneOffset == REQUEST_GPS_USAGE)
        {
            cal.setTimeZone(getGPS_Timezone());
            return cal;
        }

        if(timezoneOffset == DEFAULT_ANDROID_TIMEZONE)
        {
            cal.setTimeZone(TimeZone.getDefault());
            return cal;
        }
        if(timezoneOffset > 12 || timezoneOffset < -12)
        {
            cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        else
        {
            if(timezoneOffset == 0)
                cal.setTimeZone(TimeZone.getTimeZone("UTC"));
            else
            {
                if(timezoneOffset < 0)
                {
                    cal.setTimeZone(TimeZone.getTimeZone("GMT-"+timezoneOffset+":00"));
                }
                else {
                    cal.setTimeZone(TimeZone.getTimeZone("GMT+"+timezoneOffset+":00"));
                }
            }
        }
        return cal;
    }

}
