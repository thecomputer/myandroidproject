package com.example.liav.openmanagementsystem.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, final Intent intent) {
        Log.d("Network Changed! ", "Flag No 1");
        //Toast.makeText(context,"something",Toast.LENGTH_SHORT).show();
        boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY,false);
        if(noConnectivity)
        {
            Toast.makeText(context,"Network Unavailable ",Toast.LENGTH_SHORT).show();
            Log.d("Network Unavailable ", "Flag No 1");
            //AppDataHolder.getInstance().getData().setNetworkConnected(false);
        }
        else
        {
            Toast.makeText(context,"Network Available ",Toast.LENGTH_SHORT).show();
            Log.d("Network Available ", "Flag No 1");
            //AppDataHolder.getInstance().getData().setNetworkConnected(true);
        }

    }
    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }
}