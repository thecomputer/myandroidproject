package com.example.liav.openmanagementsystem.network;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.example.liav.openmanagementsystem.models.user_templates.NotesContract;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.CalendarContract;
import com.example.liav.openmanagementsystem.models.user_templates.timetableContract.TimetableContract;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class AsyncHTTP_Update extends AsyncTask<Void, Void, Integer> {

    private String url;

    public AsyncHTTP_Update(String url) {
        this.url = url;
    }

    protected void onPreExecute()
    {

    }


    @Override
    protected Integer doInBackground(Void... voids)
    {



        //if(type == 1)
        HttpURLConnection connection = null;
        InputStream stream = null;
        try {

            URL url = new URL(this.url);

            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
            String result = reader.readLine();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return 0;
    }

    protected void onPostExecute(Integer intd)
    {

    }

}