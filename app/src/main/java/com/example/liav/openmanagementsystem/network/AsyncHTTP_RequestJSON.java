package com.example.liav.openmanagementsystem.network;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.TimetableEventsListFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.CalendarEventsListFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments.NotesListFragment;
import com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments.NotesFragment;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class AsyncHTTP_RequestJSON extends AsyncTask<Void, Void, Integer>
{
    public static final int TIMETABLE_TYPE = 1;
    public static final int NOTES_TYPE = 2;
    public static final int CALENDAR_TYPE = 3;


    private Fragment fragment;
    private int type;
    private String url;

    public AsyncHTTP_RequestJSON(Fragment fragment, int type,String url) {
        this.type = type;
        this.fragment = fragment;
        this.url = url;
    }

    protected void onPreExecute()
    {

    }


    @Override
    protected Integer doInBackground(Void... voids)
    {
        //if(type == 1)
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(this.url);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();


            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuffer buffer = new StringBuffer();
            String line = "";

            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");
                Log.d("Response: ", "> " + line);

            }

            if(this.type == TIMETABLE_TYPE)
                UpdateTimetable(buffer.toString());
            if(this.type == CALENDAR_TYPE)
                UpdateCalendar(buffer.toString());
            if(this.type == NOTES_TYPE)
                UpdateNotes(buffer.toString());


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }

    private void UpdateTimetable(String buffer)
    {
        ArrayList<TimetableEvent> eventArrayList = new ArrayList<>();
        JSONArray response = null;
        try {
            response = new JSONArray(buffer.toString());
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject jsonObject = response.getJSONObject(i);

                    TimetableEvent t = new TimetableEvent(jsonObject.getInt("id"),false,
                            jsonObject.getString("title"),jsonObject.getString("description"),jsonObject.getString("notes")
                            ,jsonObject.getInt("day"),jsonObject.getInt("hour"),jsonObject.getInt("minute"));

                    eventArrayList.add(t);
                    Log.i("check",t.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ((TimetableEventsListFragment)fragment).setArraylist(eventArrayList);

    }

    private void UpdateNotes(String buffer)
    {
        ArrayList<Note> ArrayList = new ArrayList<>();
        JSONArray response = null;
        try {
            response = new JSONArray(buffer.toString());
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject jsonObject = response.getJSONObject(i);

                    Note t = new Note(jsonObject.getInt("id"),jsonObject.getInt("timestamp"), jsonObject.getString("title"),jsonObject.getString("description"));

                    ArrayList.add(t);
                    Log.i("check",t.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ((NotesFragment)fragment).updateNotesFromThread(ArrayList);

    }

    private void UpdateCalendar(String buffer)
    {
        ArrayList<CalendarEvent> eventArrayList = new ArrayList<>();
        JSONArray response = null;
        try {
            response = new JSONArray(buffer.toString());
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject jsonObject = response.getJSONObject(i);

                    CalendarEvent t = new CalendarEvent(jsonObject.getInt("id"),false,jsonObject.getString("title")
                    ,jsonObject.getString("description"),jsonObject.getString("notes"), jsonObject.getInt("timestamp"));

                    eventArrayList.add(t);
                    Log.i("check",t.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ((CalendarEventsListFragment)fragment).updateEventsFromThread(eventArrayList);

    }

    protected void onPostExecute(Integer intd)
    {
        if(this.type == TIMETABLE_TYPE)
        {
            ((TimetableEventsListFragment)fragment).updateExistingFromThread();
        }
        if(this.type == NOTES_TYPE)
        {
            ((NotesFragment)fragment).updateFragmentFromThread();
        }
        if(this.type == CALENDAR_TYPE)
        {
            ((CalendarEventsListFragment)fragment).updateExistingFromThread();
        }


    }
}