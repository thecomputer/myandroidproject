package com.example.liav.openmanagementsystem.fragments.user_fragments.main_fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.R;

public class ProfileFragment extends Fragment {


    private boolean ready = false;
    private String username;
    private String email;
    private String token;
    private String provider_uri;

    public void setValuesLocal()
    {
        this.email = "undefined";
        this.username = "local";
        this.provider_uri = "local://localphone/";
        this.token = "undefined";

    }

    public void setValues(String username,String email,String provider_uri,String token)
    {
        this.email = email;
        this.username = username;
        this.provider_uri = provider_uri;
        this.token = token;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);

        TextView username = v.findViewById(R.id.username);
        TextView email = v.findViewById(R.id.email);
        TextView token = v.findViewById(R.id.token);
        TextView provider_uri = v.findViewById(R.id.provider_uri);

        username.setText(this.username);
        email.setText(this.email);
        token.setText(this.token);
        provider_uri.setText(this.provider_uri);

        return v;
    }

}
