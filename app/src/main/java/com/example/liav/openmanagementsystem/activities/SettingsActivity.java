package com.example.liav.openmanagementsystem.activities;

import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;


import com.example.liav.openmanagementsystem.fragments.app_fragments.SettingsFragment;
import com.example.liav.openmanagementsystem.utils.AppDataManager;
import com.example.liav.openmanagementsystem.R;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */

public class SettingsActivity extends AppCompatActivity {
    private AppDataManager appDataManager;

    private SettingsFragment fragment;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setupActionBar();


        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.settings,new SettingsFragment()).commit();
    }
    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        if(this.fragment == null)
        {
            super.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(this.fragment == null)
        {
            super.onBackPressed();
        }
        else
        {
            this.fragment = null;
            //fragment.backPressed();
        }
    }

    private void setupActionBar() {
        //ActionBar actionBar = getActionBar();
        //if (actionBar != null) {
            // Show the Up button in the action bar.
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        //}
    }


}
