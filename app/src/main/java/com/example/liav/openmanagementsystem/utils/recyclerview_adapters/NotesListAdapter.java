package com.example.liav.openmanagementsystem.utils.recyclerview_adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;
import com.example.liav.openmanagementsystem.utils.layout.NotesListBuilder;
import com.example.liav.openmanagementsystem.utils.layout.TabletimeEventsListBuilder;

import java.util.ArrayList;


public class NotesListAdapter extends RecyclerView.Adapter {

    private ArrayList<Note> notesArrayList;

    public static class EventsViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Context context;
        public LinearLayout linearLayout;
        public TextView textTitle,textDesc;

        public EventsViewHolder(LinearLayout v, Context context) {
            super(v);
            linearLayout = v;
            this.context = context;
            textTitle = v.findViewById(R.id.main_title);
            textDesc = v.findViewById(R.id.main_desc);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public NotesListAdapter(ArrayList<Note> notes) {
        this.notesArrayList = notes;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NotesListAdapter.EventsViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        // create a new view

        LinearLayout v = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.list_note_item,
                parent,
                false);


        EventsViewHolder vh = new EventsViewHolder(v,parent.getContext());
        vh.context = parent.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        NotesListBuilder.buildNotesEvents(this.notesArrayList.get(i),
                ((EventsViewHolder)viewHolder).linearLayout,
                ((EventsViewHolder)viewHolder).context);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.notesArrayList.size();
    }
}
