//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.example.liav.openmanagementsystem.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.liav.openmanagementsystem.models.templates.AccountsContract;
import com.example.liav.openmanagementsystem.models.templates.ProvidersContract;
import com.example.liav.openmanagementsystem.models.templates.SettingsContract;
import com.example.liav.openmanagementsystem.models.user_templates.ProfileSettingsContract;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.CalendarContract;
import com.example.liav.openmanagementsystem.models.user_templates.calendarTables.MediaContract;
import com.example.liav.openmanagementsystem.models.user_templates.NotesContract;
import com.example.liav.openmanagementsystem.models.user_templates.timetableContract.TimetableContract;


public class SQLiteHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "oms_internals.db";

    public void onCreate(SQLiteDatabase db) {

        db.execSQL(AccountsContract.SQL_CREATE_ENTRIES);
        db.execSQL(ProvidersContract.SQL_CREATE_ENTRIES);
        db.execSQL(SettingsContract.SQL_CREATE_ENTRIES);

        // Create Settings Records
        for(int i=0; i< SettingsContract.DefaultSettings.length;i++)
        {
            String[] values_arr = {SettingsContract.DefaultSettings[i][0],SettingsContract.DefaultSettings[i][1],SettingsContract.DefaultSettings[i][2],SettingsContract.DefaultSettings[i][3],SettingsContract.DefaultSettings[i][4]};
            db.execSQL(SettingsContract.InsertSettingQuery(values_arr));
        }

        CreateLocalUserTables(db);
    }

    private void CreateLocalUserTables(SQLiteDatabase db)
    {
        db.execSQL(CalendarContract.SQL_CREATE_ENTRIES);
        db.execSQL(NotesContract.SQL_CREATE_ENTRIES);
        db.execSQL(TimetableContract.SQL_CREATE_ENTRIES);
        db.execSQL(ProfileSettingsContract.SQL_CREATE_ENTRIES);

    }

    private void DeleteLocalUserTables(SQLiteDatabase db)
    {
        db.execSQL(CalendarContract.SQL_DELETE_ENTRIES);
        db.execSQL(NotesContract.SQL_DELETE_ENTRIES);
        db.execSQL(TimetableContract.SQL_DELETE_ENTRIES);
        db.execSQL(ProfileSettingsContract.SQL_DELETE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(AccountsContract.SQL_DELETE_ENTRIES);
        db.execSQL(SettingsContract.SQL_DELETE_ENTRIES);
        db.execSQL(ProvidersContract.SQL_DELETE_ENTRIES);
        DeleteLocalUserTables(db);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //this.getWritableDatabase(); // no need to autoload database...
    }
}
