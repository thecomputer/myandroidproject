package com.example.liav.openmanagementsystem.utils.layout;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.example.liav.openmanagementsystem.activities.ItemActivity;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.utils.dataholders.FormDataHolder;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.example.liav.openmanagementsystem.utils.layout.DialogBuilder.setDialogCalendarEventList;
import static com.example.liav.openmanagementsystem.utils.layout.ListBuilder.buildCalendarEventList;

public class CalendarEventsListBuilder {
    private CalendarEventsListBuilder() {}



    public static void buildCalendarEvents(CalendarEvent calendarEvent,LinearLayout layout2, Context context)
    {
        Date date;
        Timestamp stamp;
        Calendar cal = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
            Log.i("logging building",""+ calendarEvent.getID());
            ListBuilder.addLayoutItemList(layout2);

            stamp = new Timestamp(calendarEvent.getTimestamp()* UserDataHolder.getInstance().getData().timezoneHelper.MILISECONDS_TO_SECOND);
            date = new Date(stamp.getTime());
            cal.setTime(date);

            setListener(layout2, calendarEvent.getID(),
                    context,
                    cal.get(Calendar.HOUR_OF_DAY),
                    cal.get(Calendar.MINUTE),
                    cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH),
                    cal.get(Calendar.DAY_OF_MONTH),
                    calendarEvent
            );
            buildListItem(layout2,context, calendarEvent);
    }

    private static void buildListItem(LinearLayout layout2,Context context,CalendarEvent calendarEvent)
    {
        Timestamp stamp = new Timestamp(calendarEvent.getTimestamp()*UserDataHolder.getInstance().getData().timezoneHelper.MILISECONDS_TO_SECOND);
        Date date = new Date(stamp.getTime());
        Calendar cal = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
        cal.setTime(date);
        buildCalendarEventList(
                layout2,
                context,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH),
                calendarEvent.getTitle(),
                calendarEvent.getDescription()
        );
    }
    private static void setListener(LinearLayout linearLayout, int i, Context context, int hour, int minute,int year,int month, int day,final CalendarEvent calendarEvent)
    {
        final Context ClassContext = context;
        final int layoutID = i;
        final int h = hour;
        final int m = minute;
        final int y = year;
        final int mon = month;
        final int d = day;
        linearLayout.setOnLongClickListener(new View.OnLongClickListener()
        {

            @Override
            public boolean onLongClick(View v) {
                String[] options = { "Edit event", "Delete event" };
                FormDataHolder.getInstance().getData().calendarEvent = calendarEvent;
                setDialogCalendarEventList(options, ClassContext,layoutID,h,m,y,mon,d);
                return true;

            }

        });

        linearLayout.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {

                Intent myIntent;
                myIntent = new Intent(ClassContext, ItemActivity.class);
                myIntent.putExtra("type", "event");
                SimpleDateFormat sdf = new SimpleDateFormat( "dd-MMM-yyyy HH:mm" );
                Log.i("date",""+h);
                Calendar calendar = UserDataHolder.getInstance().getData().timezoneHelper.getCalendar();
                Date date = new Date();
                date.setTime(calendarEvent.getTimestamp()*1000);
                calendar.setTime(date);
                //calendar.set(Calendar.HOUR_OF_DAY);
                //calendar.set(Calendar.MINUTE,m);
                //calendar.set(Calendar.DAY_OF_MONTH,d);
                //calendar.set(Calendar.MONTH,mon);
                //calendar.set(Calendar.YEAR,y);
                String time = sdf.format(calendar.getTimeInMillis());

                myIntent.putExtra("title",calendarEvent.getTitle());
                myIntent.putExtra("description",calendarEvent.getDescription());
                myIntent.putExtra("time",time);
                myIntent.putExtra("notes",calendarEvent.getNotes());

                ClassContext.startActivity(myIntent);
            }

        });


    }


}
