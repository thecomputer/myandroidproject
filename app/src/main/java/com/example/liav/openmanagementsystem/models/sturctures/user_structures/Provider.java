package com.example.liav.openmanagementsystem.models.sturctures.user_structures;

public class Provider {

    private int id;
    private String provider_name;
    private String provider_uri;
    private String hash_strength;

    public Provider(int id,String provider_name,String provider_uri,String hash_strength)
    {
        this.id = id;
        this.provider_name = provider_name;
        this.provider_uri = provider_uri;
        this.hash_strength = hash_strength;
    }

    public int getID() {
        return this.id;
    }

    public String getProvider_name() {
        return provider_name;
    }

    public String getProvider_uri() {
        return provider_uri;
    }

    public String getHash_strength() {
        return hash_strength;
    }
}
