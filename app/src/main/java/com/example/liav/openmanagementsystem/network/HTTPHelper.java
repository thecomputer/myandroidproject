//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.example.liav.openmanagementsystem.network;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;

import java.util.HashMap;

public final class HTTPHelper {
    private HTTPHelper() {}

    public static void LoginHTTP(Account account, String uri_provider, String url, Context context)
    {
        new AsyncHTTP_Login(url,uri_provider,account.getUsername(),account.getPassword(),context).execute();
    }
    public static void RequestDataHTTP(String url,int type, Fragment fragment)
    {
        new AsyncHTTP_RequestJSON(fragment,type,url).execute();
    }
    public static void PostDataHTTP(HashMap<String,String> values,String url, int type)
    {
        new AsyncHTTP_Post(values,type,url).execute();
    }
    public static void UpdateDataHTTP(String url)
    {
        new AsyncHTTP_Update(url).execute();
    }

}
