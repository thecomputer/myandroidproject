package com.example.liav.openmanagementsystem.utils.dataholders;

import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.TimetableEvent;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Account;
import com.example.liav.openmanagementsystem.models.sturctures.user_structures.Provider;


public class FormData {
    public Note note;
    public CalendarEvent calendarEvent;
    public TimetableEvent timetableEvent;
    public Provider provider;
    public Account account;

}
