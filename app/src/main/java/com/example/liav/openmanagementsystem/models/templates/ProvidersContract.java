//Copyright 2018 Liav Albani
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
package com.example.liav.openmanagementsystem.models.templates;

import android.content.ContentValues;
import android.provider.BaseColumns;

public final class ProvidersContract {

    private ProvidersContract() {}
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "Providers";
        public static final String COLUMN1_NAME_TITLE = "provider_name";
        public static final String COLUMN2_NAME_TITLE = "provider_uri";
        public static final String COLUMN3_NAME_TITLE = "hash_strength";
    }

    public static final String[] columns = {FeedEntry.COLUMN1_NAME_TITLE,FeedEntry.COLUMN2_NAME_TITLE,FeedEntry.COLUMN3_NAME_TITLE};

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN1_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN2_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN3_NAME_TITLE + " TEXT);";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    public static ContentValues PrepareWriteToTable(String[] values_arr)
    {
        if(values_arr.length != 3)
            return null;
// Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedEntry.COLUMN1_NAME_TITLE, values_arr[0]);
        values.put(FeedEntry.COLUMN2_NAME_TITLE, values_arr[1]);
        values.put(FeedEntry.COLUMN3_NAME_TITLE, values_arr[2]);
// Insert the new row, returning the primary key value of the new row
        return values;
    }
}
