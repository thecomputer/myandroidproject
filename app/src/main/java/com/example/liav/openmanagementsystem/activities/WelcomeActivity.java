package com.example.liav.openmanagementsystem.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.utils.AppDataManager;
import com.example.liav.openmanagementsystem.utils.dataholders.AppDataHolder;
import com.example.liav.openmanagementsystem.utils.dataholders.FormData;
import com.example.liav.openmanagementsystem.utils.dataholders.FormDataHolder;
import com.example.liav.openmanagementsystem.utils.layout.WelcomeScreenTask;
import com.example.liav.openmanagementsystem.utils.timezone.GPSHelper;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    public void onPause() {

        super.onPause();
        overridePendingTransition(0, 0);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);

        new WelcomeScreenTask(this,progressBar).execute();
    }

    @Override
    protected void onRestart() {
        moveTaskToBack(true);
        finish();
        super.onRestart();
    }
}
