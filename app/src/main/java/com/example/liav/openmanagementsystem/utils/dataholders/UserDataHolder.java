package com.example.liav.openmanagementsystem.utils.dataholders;

import com.example.liav.openmanagementsystem.utils.UserDataManager;

public class UserDataHolder {
    private UserDataManager data;
    public UserDataManager getData() {return data;}
    public void setData(UserDataManager data) {this.data = data;}

    private static final UserDataHolder holder = new UserDataHolder();
    public static UserDataHolder getInstance() {return holder;}
}
