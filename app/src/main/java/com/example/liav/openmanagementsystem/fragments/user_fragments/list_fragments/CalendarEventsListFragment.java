package com.example.liav.openmanagementsystem.fragments.user_fragments.list_fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.liav.openmanagementsystem.R;
import com.example.liav.openmanagementsystem.activities.InputActivity;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.CalendarEvent;
import com.example.liav.openmanagementsystem.models.sturctures.general_structures.Note;
import com.example.liav.openmanagementsystem.models.templates.EventsListFragmentTypes;
import com.example.liav.openmanagementsystem.models.templates.InputActivityTypes;
import com.example.liav.openmanagementsystem.utils.dataholders.UserDataHolder;
import com.example.liav.openmanagementsystem.utils.recyclerview_adapters.CalendarEventsAdapter;

import java.util.ArrayList;

public class CalendarEventsListFragment extends Fragment {

    private boolean hasLoadedOnce = false;
    private int day;
    private Context context;
    private boolean eventsEmpty = true;


    private View inf;
    private int type;
    private int[] params;
    private ArrayList<CalendarEvent> calendarEvents;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private void LoadCalendarEvents()
    {
        ArrayList<CalendarEvent> calendarEvents=new ArrayList<>();
        switch (this.type)
        {
            case EventsListFragmentTypes.CalendarEventsAll._CLASS: // All events schedule fragment
                calendarEvents = UserDataHolder.getInstance().getData().getAllCalendarEvent(this);
                break;
            case EventsListFragmentTypes.CalendarEventsToday._CLASS: // All events schedule fragment
                calendarEvents = UserDataHolder.getInstance().getData().getTodayCalendarEvent(this);
                break;
            case EventsListFragmentTypes.CalendarEventsDay._CLASS: // Day events schedule fragment
                calendarEvents = UserDataHolder.getInstance().getData().getDayCalendarEvent(params[0],params[1],params[2],this);
                break;
        }
        this.calendarEvents = calendarEvents;

    }

    public void updateEventsFromThread(ArrayList<CalendarEvent> events)
    {
        this.calendarEvents = events;
    }


    public void setType(int type, int params[])
    {
        this.type = type;
        this.params = params;
        LoadCalendarEvents();

    }
    public CalendarEventsListFragment()
    {
        super();

    }

    @Override
    public void onDetach() {
        this.context = null;
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        this.context = context;

        super.onAttach(context);

    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible) {
        super.setUserVisibleHint(true);

        if (this.isVisible()) {
            if (isFragmentVisible && hasLoadedOnce) {
                // start update code here so this code run only when user select this fragment
                this.updateExisting();
            }
        }
    }

    private Context getClassContext()
    {
        return this.context;
    }

    private void setListenerFloatingActionButton(View inf)
    {
        FloatingActionButton button = inf.findViewById(R.id.fab);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent myIntent;

                myIntent = new Intent(getActivity(), InputActivity.class);
                myIntent.putExtra("type", InputActivityTypes.CLASS_B_CREATE._NAME);
                myIntent.putExtra("subtype", InputActivityTypes.CLASS_B_CREATE.CALENDAR_EVENT);
                startActivity(myIntent);
            }
        });
    }



    private void PrintEvents(View inf)
    {
        FloatingActionButton button = inf.findViewById(R.id.fab);

        switch(this.type)
        {
            case EventsListFragmentTypes.CalendarEventsToday._CLASS:
                button.hide();
                setRecyclerViewList(inf);
                break;
            case EventsListFragmentTypes.CalendarEventsAll._CLASS:
                setRecyclerViewList(inf);
                break;
            case EventsListFragmentTypes.CalendarEventsDay._CLASS:
                button.hide();
                setRecyclerViewList(inf);
                break;
        }

    }

    private void setRecyclerViewList(View inf)
    {
        recyclerView = (RecyclerView) inf.findViewById(R.id.eventsList);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(false);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this.context);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new CalendarEventsAdapter(this.calendarEvents);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        setListenerFloatingActionButton(inf);
    }

    public void updateExisting()
    {
        Log.i("attaching","updating!");
        if(this.hasLoadedOnce)
        {
            this.LoadCalendarEvents();
            this.update();
        }
    }

    public void updateExistingFromThread()
    {
        Log.i("attaching","updating!");
        if(this.hasLoadedOnce)
        {
            this.PrintEvents(this.inf);
        }
    }

    public void update()
    {
        PrintEvents(this.inf);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View inf = inflater.inflate(R.layout.fragment_events, container, false);
        this.inf = inf;

        this.update();

        this.hasLoadedOnce = true;

        return inf;

    }



}
