package com.example.liav.openmanagementsystem.utils.timezone;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

public class GPSHelper {

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    public static Location location; // location

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;
    //Location locManager = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

    private Context mContext;

    public GPSHelper() {

    }
    /**
     * Function to get latitude
     * */
    public static double getLatitude(){
        double latitude=0;
        if(location != null){
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     * */
    public static double getLongitude(){
        double longitude=0;
        if(location != null){
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }


}
